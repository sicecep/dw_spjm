/*
Navicat MySQL Data Transfer

Source Server         : localhost
Source Server Version : 50617
Source Host           : 127.0.0.1:3306
Source Database       : dw_spjm

Target Server Type    : MYSQL
Target Server Version : 50617
File Encoding         : 65001

Date: 2016-05-08 14:03:11
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `dim_jurusanfakultas`
-- ----------------------------
DROP TABLE IF EXISTS `dim_jurusanfakultas`;
CREATE TABLE `dim_jurusanfakultas` (
  `Kode_JurusanFakultas` char(7) NOT NULL,
  `Nama_Jurusan` varchar(50) NOT NULL,
  `Nama_Fakultas` varchar(50) NOT NULL,
  PRIMARY KEY (`Kode_JurusanFakultas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of dim_jurusanfakultas
-- ----------------------------
INSERT INTO `dim_jurusanfakultas` VALUES ('D3AKFEK', 'D3 AKUNTANSI ', 'FAKULTAS EKONOMI ');
INSERT INTO `dim_jurusanfakultas` VALUES ('D3FSFKS', 'D3 FISIOTERAPI', 'FAKULTAS ILMU - ILMU KESEHATAN');
INSERT INTO `dim_jurusanfakultas` VALUES ('D3KBFEK', 'D3 KEUANGAN PERBANKAN', 'FAKULTAS EKONOMI');
INSERT INTO `dim_jurusanfakultas` VALUES ('D3PRFKS', 'D3 KEPERAWATAN ', 'FAKULTAS ILMU - ILMU KESEHATAN');
INSERT INTO `dim_jurusanfakultas` VALUES ('D3SIFIK', 'D3 SISTEM INFORMASI ', 'FAKULTAS ILMU KOMPUTER');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1AKFEK', 'S1 AKUNTANSI ', 'FAKULTAS EKONOMI');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1DUFKD', 'S1 KEDOKTERAN UMUM', 'FAKULTAS KEDOKTERAN');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1GZFKS', 'S1 ILMU GIZI', 'FAKULTAS ILMU - ILMU KESEHATAN');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1HIFSP', 'S1 HUBUNGAN INTERNASIONAL ', 'FAKULTAS ILMU SOSIAL DAN POLITIK');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1IHFHK', 'S1 ILMU HUKUM', 'FAKULTAS HUKUM');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1IKFSP', 'S1 ILMU KOMUNIKASI ', 'FAKULTAS ILMU SOSIAL DAN POLITIK');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1KMFKS', 'S1 KESEHATAN MASYARAKAT', 'FAKULTAS ILMU - ILMU KESEHATAN');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1MAFEK', 'S1 MANAJEMEN', 'FAKULTAS EKONOMI ');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1PDFKD', 'S1 PROFESI DOKTER', 'FAKULTAS KEDOKTERAN');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1PNFKS', 'S1 PROFESI NERS', 'FAKULTAS ILMU - ILMU KESEHATAN');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1PRFKS', 'S1 KEPERAWATAN', 'FAKULTAS ILMU - ILMU KESEHATAN');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1SIFIK', 'S1 SISTEM INFORMASI', 'FAKULTAS ILMU KOMPUTER');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1TDFTK', 'S1 TEKNIK INDUSTRI ', 'FAKULTAS TEKNIK');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1TIFIK', 'S1 TEKNIK INFORMATIKA', 'FAKULTAS ILMU KOMPUTER');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1TMFTK', 'S1 TEKNIK MESIN ', 'FAKULTAS TEKNIK');
INSERT INTO `dim_jurusanfakultas` VALUES ('S1TPFTK', 'S1 TEKNIK PERKAPALAN ', 'FAKULTAS TEKNIK');

-- ----------------------------
-- Table structure for `fact_laporan1`
-- ----------------------------
DROP TABLE IF EXISTS `fact_laporan1`;
CREATE TABLE `fact_laporan1` (
  `NIDN` int(10) NOT NULL,
  `Kode_JurusanFakultas` char(7) NOT NULL,
  `Nama_Dosen` varchar(50) NOT NULL,
  `Jabatan_Akademik` varchar(50) NOT NULL,
  `Jabatan_Usulan` varchar(50) NOT NULL,
  `Nilai_Kum_Akhir` int(4) NOT NULL,
  `Periode_Pengajuan` char(50) NOT NULL,
  `Presentase_Kum_Pengajaran` decimal(10,0) NOT NULL,
  `Persentase_Kum_Penelitian` decimal(10,0) NOT NULL,
  `Persentase_Kum_PengMasy` decimal(10,0) NOT NULL,
  `Persentase_Kum_UnsurPenunjang` decimal(10,0) NOT NULL,
  `Kode_Tahun_Pengajuan` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`NIDN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of fact_laporan1
-- ----------------------------
INSERT INTO `fact_laporan1` VALUES ('27017209', 'S1IHFHK', 'KAYUS K LEWOLEBA', '', 'Asisten Ahli ', '0', 'Ganjil 2015', '0', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('27017210', 'S1IHFHK', 'Intan Dalimunte, SH.,MH', 'Asisten Ahli', 'Lektor 200', '150', 'Ganjil 2015', '55', '47', '10', '10', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('27017219', 'S1IHFHK', 'Rika Amaliha, SH.,MH', '', 'Asisten Ahli ', '0', 'Ganjil 2015', '0', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('27017604', 'S1IHFHK', 'Slyvana, SH.,MH', '', 'Asisten Ahli ', '0', 'Ganjil 2015', '0', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('27018603', 'S1IHFHK', 'Slamet Tri Wahyudi, SH.,MH', '', 'Asisten Ahli ', '0', 'Ganjil 2015', '0', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('27018605', 'S1IHFHK', 'Dika Saputra, SH.,MH', 'Asisten Ahli', 'Lektor 200 ', '150', 'Ganjil 2015', '57', '45', '12', '10', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('302086903', 'S1PNFKS', 'Tatiana Siregar.,SKep.,MM', '', 'Asisten Ahli ', '0', 'Ganjil 2015', '0', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('302086904', 'S1PRFKS', 'Ika Marbela Sari.,SKep.,MM', 'Asisten Ahli', 'Lektor 200 ', '150', 'Ganjil 2015', '50', '48', '15', '15', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('302087907', 'S1GZFKS', 'Timbul Akara.,SKep.,MM', 'Asisten Ahli', 'Lektor 200 ', '150', 'Ganjil 2015', '55', '47', '15', '10', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('306127901', 'S1PNFKS', 'Suryani Maryam,S.Kep.,MM', '', 'Asisten Ahli ', '0', 'Ganjil 2015', '0', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('306128906', 'S1PRFKS', 'Rahman,S.Kep.,MM', '', 'Asisten Ahli ', '0', 'Ganjil 2015', '0', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('306187912', 'S1GZFKS', 'Sri Rahmah,S.Kep.,MM', 'Asisten Ahli', 'Lektor 200 ', '150', 'Ganjil 2015', '55', '48', '10', '10', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('309118104', 'S1TIFIK', 'Henki Bayu Seta,S.Kom.,MTI', 'Asisten Ahli ', 'Lektor 200 ', '150', 'Ganjil 2015', '57', '48', '17', '15', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('324068203', 'S1SIFIK', 'PAULUS PRANANTO, MSc', 'Lektor Kepala 400', 'Lektor Kepala 550', '400', 'Genap  2005', '67', '13', '10', '7', '1', '2016-04-19 16:23:24', '2016-04-24 12:33:07');
INSERT INTO `fact_laporan1` VALUES ('412119503', 'S1TIFIK', 'Vini Indriasari, Ph.D', '', 'Asisten Ahli ', '0', 'Ganjil 2015', '0', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('420018601', 'S1SIFIK', 'Rio Wirawan,S.Kom.,MMSI', '', 'Asisten Ahli ', '0', 'Ganjil 2015', '0', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('421019501', 'S1SIFIK', 'Catur Nugrahaeni PD., M.Kom', '', 'Asisten Ahli ', '0', 'Ganjil 2015', '0', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('425220502', 'S1TIFIK', 'Bayu Hananto.,M.Kom', '', 'Asisten Ahli ', '0', 'Ganjil 2015', '0', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan1` VALUES ('426619620', 'D3SIFIK', 'Nur Hafifah Martondang,S.Kom.MM', '', 'Asisten Ahli ', '0', 'Ganjil 2015', '0', '0', '0', '0', '1', null, null);

-- ----------------------------
-- Table structure for `fact_laporan2_3`
-- ----------------------------
DROP TABLE IF EXISTS `fact_laporan2_3`;
CREATE TABLE `fact_laporan2_3` (
  `NIDN` varchar(10) NOT NULL,
  `Kode_JurusanFakultas` char(7) NOT NULL,
  `Nama_Dosen` varchar(50) NOT NULL,
  `Jabatan_Akademik` varchar(50) NOT NULL,
  `Pendidikan` char(2) NOT NULL,
  `Kum_Pengajaran` double NOT NULL,
  `Kum_Penelitian` double NOT NULL,
  `Kum_PengMasy` double NOT NULL,
  `Kum_UnsurPenunjang` double NOT NULL,
  `Kode_Tahun_Pengajuan` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`NIDN`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of fact_laporan2_3
-- ----------------------------
INSERT INTO `fact_laporan2_3` VALUES ('0025116101', 'D3FSFKS', 'dr.Dani Purnomo,M.Kes', 'Lektor Kepala', 'S3', '90', '45', '25', '25', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0025116109', 'S1SIFIK', 'Dr.Dwi Nugroho, MSc', 'Lektor Kepala', 'S3', '90', '45', '25', '25', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0301094201', 'S1TIFIK', 'Sukirno,Ir. MSc', 'Lektor Kepala', 'S2', '111.5', '113', '15', '15', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0301094202', 'D3FSFKS', 'Wayan P,M.Kes', 'Lektor Kepala', 'S2', '112', '113', '15', '15', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0306077801', 'S1TIFIK', 'Goldie Gunadi,S.Kom, M.Kom', 'Asisten Ahli', 'S2', '11', '10', '3', '2', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0306077802', 'D3PRFKS', 'Gunawan,M.Kes', 'Asisten Ahli', 'S2', '11', '10', '3', '2', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0307036401', 'S1TIFIK', 'Aries Herawat,iS.Kom, MSi', 'Asisten Ahli ', 'S2', '11', '10', '3', '3', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0307036402', 'D3PRFKS', 'Maryani.,M.Kes', 'Asisten Ahli ', 'S2', '12', '10', '4', '3', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0307046401', 'S1SIFIK', 'Sayuti,M.Kom', 'Asisten Ahli', 'S2', '11', '10', '3', '2', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0307046402', 'S1GZFKS', 'Samiyati,M.Kes', 'Asisten Ahli', 'S2', '11', '10', '3', '5', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0308046901', 'S1SIFIK', 'Titin Pramiyati,S.Kom, MSi', 'Asisten Ahli ', 'S2', '23', '10', '5', '5', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0308046902', 'S1GZFKS', ' Pramiyati,M.Kes', 'Asisten Ahli ', 'S2', '23', '15', '5', '6', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0309118102', 'S1KMFKS', 'Henki.M.Kes', 'Asisten Ahli', 'S2', '12', '11', '4', '4', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0309118104', 'S1TIFIK', 'Henki Bayu Seta,S.Kom.,MTI', 'Asisten Ahli', 'S2', '11', '10', '3', '4', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0313083901', 'S1TIFIK', 'Bambang Hutomo,Ir, .BcTT', 'Lektor', 'S2', '49.5', '35', '10', '10', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0313083902', 'S1PNFKS', 'Bambang Hutomo', 'Lektor', 'S2', '49.5', '35', '10', '10', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0320037401', 'S1TIFIK', 'Bambang Tri Wahyono,S.Kom, MSi', 'Asisten Ahli', 'S2', '33', '30', '3', '4', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0320037402', 'S1PNFKS', 'Tri Wahyono', 'Asisten Ahli', 'S2', '34', '30', '3', '3', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0321027401', 'S1SIFIK', 'Ati Zaidiah,s.Kom.,MTI', 'Asisten Ahli ', 'S2', '22', '10', '6', '4', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0321027402', 'S1PRFKS', 'Zaidiah', 'Asisten Ahli ', 'S2', '22', '10', '5', '4', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0321057001', 'S1SIFIK', 'Anita Muliawati.S.Kom.,MTI', 'Asisten Ahli ', 'S2', '16.5', '10', '3', '3', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0321057002', 'S1IHFHK', 'Anita,SH.MH', 'Asisten Ahli ', 'S2', '17', '10', '4', '3', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0324068202', 'S1PRFKS', 'Theresiawati,M.Kes', 'Asisten Ahli', 'S2', '21', '10', '4', '3', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0324068203', 'S1SIFIK', 'PAULUS PRANANTO, MSc', 'Lektor Kepala 400', 'S3', '100', '20', '15', '10', '1', null, '2016-04-24 12:33:07');
INSERT INTO `fact_laporan2_3` VALUES ('0326067501', 'S1TIFIK', 'Yuni Widiastiwi,S.Kom, MSi', 'Lektor', 'S2', '27.5', '20', '3', '4', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0326067502', 'D3SIFIK', 'Erly Krisnanik,S.Kom, MM', 'Lektor 200', 'S2', '31', '20', '5', '5', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0326067503', 'S1PRFKS', 'Erly M.Kes', 'Lektor 200', 'S2', '31', '20', '4', '4', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0326067504', 'S1PRFKS', 'Yuni Widiastiwi', 'Lektor', 'S2', '28', '20', '3', '3', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0327034701', 'S1SIFIK', 'Paulus Prananto.,MSc', 'Lektor Kepala 400', 'S2', '115.5', '113', '15', '15', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0327034702', 'S1PRFKS', 'Paulus', 'Lektor Kepala 400', 'S2', '116', '113', '15', '15', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0330096102', 'S1TIFIK', 'Jayanta,S.Kom, MSi', 'Asisten Ahli', 'S2', '22', '10', '4', '5', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0330096103', 'S1KMFKS', 'Jayanta', 'Asisten Ahli', 'S2', '22', '10', '4', '5', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0334561091', 'S1IHFHK', 'Mira, SH.MH ', 'Asisten Ahli ', 'S2', '23', '10', '4', '4', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0420018601', 'S1SIFIK', 'Rio Wirawan,S.Kom.,MMSI', '', 'S2', '22', '10', '2', '2', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('0421019501', 'S1SIFIK', 'Catur Nugrahaeni PD., M.Kom', '', 'S2', '15', '10', '10', '2', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('27017209', 'S1IHFHK', 'KAYUS K LEWOLEBA,SH.,MH', '', 'S2', '22', '10', '2', '2', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('27017210', 'S1IHFHK', 'Intan Dalimunte,SH.MH ', 'Asisten Ahli ', 'S2', '48', '35', '10', '10', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('27017219', 'S1IHFHK', 'Rika Amaliha, SH.,MH', '', 'S2', '22', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('27017604', 'S1IHFHK', 'Slyvana, SH.,MH', '', 'S2', '15', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('27018603', 'S1IHFHK', 'Slamet Tri Wahyudi, SH.,MH', '', 'S2', '15', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('27018605', 'S1IHFHK', 'Dika Saputra, SH.,MH', 'Asisten Ahli ', 'S2', '47', '35', '10', '10', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('302086903', 'S1PNFKS', 'Tatiana Siregar.,SKep.,MM', '', 'S2', '15', '0', '0', '0', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('302086904', 'S1PNFKS', 'Ika Marbela Sari.,SKep.,MM', 'Asisten Ahli', 'S2', '48', '35', '10', '10', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('302087907', 'S1GZFKS', 'Timbul Akara.,SKep.,MM', 'Asisten Ahli ', 'S2', '48', '30', '10', '9', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('306127901', 'S1PNFKS', 'Suryani Maryam,S.Kep.,MM', '', 'S2', '20', '10', '2', '1', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('306128906', 'S1PRFKS', 'Rahman,S.Kep.,MM', '', 'S2', '15', '10', '0', '1', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('306187912', 'S1GZFKS', 'Sri Rahmah,S.Kep.,MM', 'Asisten Ahli ', 'S2', '28', '20', '10', '8', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('412119503', 'S1TIFIK', 'Vini Indriasari, M.Kom', '', 'S2', '25', '10', '0', '4', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('425220502', 'S1TIFIK', 'Bayu Hananto.,M.Kom', '', 'S2', '10', '0', '0', '3', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('426619620', 'D3SIFIK', 'Nur Hafifah Martondang,S.Kom.MM', '', 'S2', '16', '0', '2', '3', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('9803020091', 'S1IHFHK', 'Syahrani Bakrie,SH,MH', 'Asisten Ahli', 'S2', '11', '10', '3', '2', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('990301531', 'S1IHFHK', 'Andri BL, SH.MH', 'Asisten Ahli ', 'S2', '17', '10', '6', '5', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('990301537', 'S1SIFIK', 'Andri Sunata,S.Kom, MM, M.TI', 'Asisten Ahli ', 'S2', '16.5', '10', '6', '3', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('9903020091', 'S1IHFHK', ' Bakrie S.H.,MH', 'Asisten Ahli', 'S2', '18', '10', '4', '3', '1', null, null);
INSERT INTO `fact_laporan2_3` VALUES ('9903020095', 'S1IHFHK', ' BaniS.H.,MH', 'Asisten Ahli', 'S2', '22', '10', '2', '2', '1', null, null);

-- ----------------------------
-- Table structure for `groups`
-- ----------------------------
DROP TABLE IF EXISTS `groups`;
CREATE TABLE `groups` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `groups_name_unique` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of groups
-- ----------------------------
INSERT INTO `groups` VALUES ('1', 'admin', '{\"superuser\":1}', '2015-04-22 03:44:14', '2015-04-22 03:44:14');
INSERT INTO `groups` VALUES ('2', 'dekan', '{\"users.edit\":1,\"users.delete\":1,\"lists.create\":1,\"lists.edit\":1,\"lists.delete\":1}', '2015-04-22 03:44:14', '2015-04-22 03:44:14');
INSERT INTO `groups` VALUES ('3', 'kaprodi D3', '{\"users.edit\":1,\"users.delete\":1,\"lists.create\":1,\"lists.edit\":1,\"lists.delete\":1}', '2015-04-22 03:44:14', '2015-04-22 03:44:14');
INSERT INTO `groups` VALUES ('4', 'kaprodi SI', '{\"users.edit\":1,\"users.delete\":1,\"lists.create\":1,\"lists.edit\":1,\"lists.delete\":1}', '2015-04-22 03:44:14', '2015-04-22 03:44:14');
INSERT INTO `groups` VALUES ('5', 'kaprodi TI', '{\"users.edit\":1,\"users.delete\":1,\"lists.create\":1,\"lists.edit\":1,\"lists.delete\":1}', '2015-04-22 03:44:14', '2015-04-22 03:44:14');
INSERT INTO `groups` VALUES ('6', 'rektor', '{\"users.edit\":1,\"users.delete\":1,\"lists.create\":1,\"lists.edit\":1,\"lists.delete\":1}', '2015-04-22 03:44:14', '2015-04-22 03:44:14');

-- ----------------------------
-- Table structure for `menus`
-- ----------------------------
DROP TABLE IF EXISTS `menus`;
CREATE TABLE `menus` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `menu` varchar(100) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `parent` int(11) DEFAULT '0',
  `order` int(11) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `level` int(11) DEFAULT NULL COMMENT 'unused',
  `active` varchar(3) DEFAULT '1' COMMENT 'unused',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of menus
-- ----------------------------
INSERT INTO `menus` VALUES ('1', 'Dashboard', 'fa-dashboard', '0', '1', null, '0', '1', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('2', 'Dosen', 'fa-laptop', '0', '2', null, '0', '1', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('3', 'Data Dosen', 'fa-circle-o', '2', '1', 'dosen', '1', '1', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('4', 'Add New', 'fa-circle-o', '2', '2', 'dosen/create', '1', '1', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('5', 'Categories', 'fa-circle-o', '2', '3', null, '1', '0', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('6', 'Tags', 'fa-circle-o', '2', '4', null, '1', '0', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('7', 'Users', 'fa-laptop', '0', '3', null, '0', '1', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('8', 'All Users', 'fa-circle-o', '7', '1', null, '1', '1', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('9', 'Add New', 'fa-circle-o', '7', '2', null, '1', '1', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('10', 'Groups', 'fa-dashboard', '0', '4', null, '0', '1', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('11', 'All Groups', 'fa-circle-o', '10', '1', 'group/index', '1', '1', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('12', 'Add New', 'fa-circle-o', '10', '2', 'group/create', '1', '1', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('13', 'Menus', 'fa-dashboard', '0', '5', null, '0', '1', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('14', 'Add New', 'fa-circle-o', '13', '1', null, '1', '1', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('15', 'Permissions', 'fa-circle-o', '13', '2', null, '1', '1', '2015-04-21 10:36:53', '2015-04-21 10:36:53');
INSERT INTO `menus` VALUES ('16', 'Tes Menu', 'fa-dashboard', '0', '6', null, '0', '1', '2015-04-22 11:33:53', '2015-04-22 11:33:53');
INSERT INTO `menus` VALUES ('17', 'Level 1', 'fa-circle-o', '16', '1', null, '1', '1', '2015-04-22 11:33:53', '2015-04-22 11:33:53');
INSERT INTO `menus` VALUES ('18', 'Level 2', 'fa-circle-o', '16', '2', null, '2', '1', '2015-04-22 11:33:53', '2015-04-22 11:33:53');
INSERT INTO `menus` VALUES ('19', 'Level 3', 'fa-circle-o', '16', '3', null, '3', '1', '2015-04-22 11:33:53', '2015-04-22 11:33:53');
INSERT INTO `menus` VALUES ('20', 'Level 1b', 'fa-circle-o', '17', '4', null, '1', '1', '2015-04-22 11:33:53', '2015-04-22 11:33:53');

-- ----------------------------
-- Table structure for `roles`
-- ----------------------------
DROP TABLE IF EXISTS `roles`;
CREATE TABLE `roles` (
  `group_id` int(10) unsigned NOT NULL DEFAULT '0',
  `menu_id` int(10) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`group_id`,`menu_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of roles
-- ----------------------------
INSERT INTO `roles` VALUES ('1', '1');
INSERT INTO `roles` VALUES ('1', '2');
INSERT INTO `roles` VALUES ('1', '3');
INSERT INTO `roles` VALUES ('1', '4');
INSERT INTO `roles` VALUES ('1', '5');
INSERT INTO `roles` VALUES ('1', '6');
INSERT INTO `roles` VALUES ('1', '7');
INSERT INTO `roles` VALUES ('1', '8');
INSERT INTO `roles` VALUES ('1', '9');
INSERT INTO `roles` VALUES ('1', '10');
INSERT INTO `roles` VALUES ('1', '11');
INSERT INTO `roles` VALUES ('1', '12');
INSERT INTO `roles` VALUES ('1', '13');
INSERT INTO `roles` VALUES ('1', '14');
INSERT INTO `roles` VALUES ('1', '15');
INSERT INTO `roles` VALUES ('1', '16');
INSERT INTO `roles` VALUES ('1', '17');
INSERT INTO `roles` VALUES ('1', '18');
INSERT INTO `roles` VALUES ('1', '19');
INSERT INTO `roles` VALUES ('1', '20');

-- ----------------------------
-- Table structure for `tahun`
-- ----------------------------
DROP TABLE IF EXISTS `tahun`;
CREATE TABLE `tahun` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `tahun` varchar(4) DEFAULT NULL,
  `deskripsi` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of tahun
-- ----------------------------
INSERT INTO `tahun` VALUES ('1', '2015', 'Gasal Tahun 2015');
INSERT INTO `tahun` VALUES ('2', '2015', 'Genap Tahun 2015');

-- ----------------------------
-- Table structure for `throttle`
-- ----------------------------
DROP TABLE IF EXISTS `throttle`;
CREATE TABLE `throttle` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned DEFAULT NULL,
  `ip_address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `attempts` int(11) NOT NULL DEFAULT '0',
  `suspended` tinyint(1) NOT NULL DEFAULT '0',
  `banned` tinyint(1) NOT NULL DEFAULT '0',
  `last_attempt_at` timestamp NULL DEFAULT NULL,
  `suspended_at` timestamp NULL DEFAULT NULL,
  `banned_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `throttle_user_id_index` (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of throttle
-- ----------------------------
INSERT INTO `throttle` VALUES ('1', '1', null, '0', '0', '0', null, null, null);
INSERT INTO `throttle` VALUES ('2', '6', null, '0', '0', '0', null, null, null);
INSERT INTO `throttle` VALUES ('3', '3', null, '0', '0', '0', null, null, null);
INSERT INTO `throttle` VALUES ('4', '2', null, '0', '0', '0', null, null, null);
INSERT INTO `throttle` VALUES ('5', '8', null, '0', '0', '0', null, null, null);
INSERT INTO `throttle` VALUES ('6', '9', null, '0', '0', '0', null, null, null);
INSERT INTO `throttle` VALUES ('7', '7', null, '0', '0', '0', null, null, null);

-- ----------------------------
-- Table structure for `users`
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `permissions` text COLLATE utf8_unicode_ci,
  `avatar` varchar(255) COLLATE utf8_unicode_ci DEFAULT '7505d64a54e061b7acd54ccd58b49dc43500b635.png',
  `activated` tinyint(1) NOT NULL DEFAULT '0',
  `activation_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `activated_at` timestamp NULL DEFAULT NULL,
  `last_login` timestamp NULL DEFAULT NULL,
  `persist_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `reset_password_code` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `first_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `last_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_email_unique` (`email`),
  KEY `users_activation_code_index` (`activation_code`),
  KEY `users_reset_password_code_index` (`reset_password_code`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'sepry', 'sepry@kudo.co.id', '$2y$10$uUj3/jRNOBfAYnO/W.O9gOgkKZimJ98y/qU3M2.greyRKGCUQXrCe', null, '7505d64a54e061b7acd54ccd58b49dc43500b635.png', '1', null, null, '2016-05-08 06:54:41', '$2y$10$k5nCYF.5xQjFtHLSdJHHa.huhT8HpzNs2TLu20s./5HuqWZx1If0m', null, null, null, '2015-04-13 10:48:07', '2016-05-08 06:54:41');
INSERT INTO `users` VALUES ('2', 'admin', 'admin@gmail.com', '$2y$10$uUj3/jRNOBfAYnO/W.O9gOgkKZimJ98y/qU3M2.greyRKGCUQXrCe', null, '7505d64a54e061b7acd54ccd58b49dc43500b635.png', '1', null, null, '2016-05-08 06:54:48', '$2y$10$UStuyNe6KOXwqZYZ6YvqTO8Fem90d9sKJJuyZGhyDNiaT.3luTCwS', null, 'Arsandi', 'Yudiyanto', '2015-04-21 04:13:45', '2016-05-08 06:54:48');
INSERT INTO `users` VALUES ('3', 'kaprodi_d3', 'kaprodi_d3@gmail.com', '$2y$10$uUj3/jRNOBfAYnO/W.O9gOgkKZimJ98y/qU3M2.greyRKGCUQXrCe', null, '7505d64a54e061b7acd54ccd58b49dc43500b635.png', '1', null, null, '2016-05-08 06:55:30', '$2y$10$WZTXXzGCO5LOHf7AgIuKsO/7m4Jqr79t1AwOOAp1tVo3J5uyzqw5S', null, null, null, '0000-00-00 00:00:00', '2016-05-08 06:55:30');
INSERT INTO `users` VALUES ('6', 'kaprodi_si', 'kaprodi_si@gmail.com', '$2y$10$uUj3/jRNOBfAYnO/W.O9gOgkKZimJ98y/qU3M2.greyRKGCUQXrCe', null, '7505d64a54e061b7acd54ccd58b49dc43500b635.png', '1', null, null, '2016-05-08 06:55:41', '$2y$10$f7KbuaMEki1wjNw22/NRIuH.3o.9B8EAkZTiZ66JewKnq0uJYKAmG', null, 'lima lima', '', '2015-11-23 15:58:19', '2016-05-08 06:55:41');
INSERT INTO `users` VALUES ('7', 'kaprodi_ti', 'kaprodi_ti@gmail.com', '$2y$10$uUj3/jRNOBfAYnO/W.O9gOgkKZimJ98y/qU3M2.greyRKGCUQXrCe', null, '7505d64a54e061b7acd54ccd58b49dc43500b635.png', '1', null, null, '2016-05-08 07:02:21', '$2y$10$SAVN2Kr2itDEpMvrDBKK9e0.FSW1Y85zuOTrkXty9wjIqHKnTprDm', null, '66666', '', '2015-11-23 16:00:42', '2016-05-08 07:02:21');
INSERT INTO `users` VALUES ('8', 'dekan', 'dekan@gmail.com', '$2y$10$uUj3/jRNOBfAYnO/W.O9gOgkKZimJ98y/qU3M2.greyRKGCUQXrCe', null, '7505d64a54e061b7acd54ccd58b49dc43500b635.png', '1', null, null, '2016-05-08 07:02:46', '$2y$10$iuBnv/GBSA5h/Pe4zqEcbeLIwlJ2ry8wP/mKwfz9ZAG.fEwWf2HMq', null, null, null, '0000-00-00 00:00:00', '2016-05-08 07:02:46');
INSERT INTO `users` VALUES ('9', 'rektor', 'rektor@gmail.com', '$2y$10$uUj3/jRNOBfAYnO/W.O9gOgkKZimJ98y/qU3M2.greyRKGCUQXrCe', null, '7505d64a54e061b7acd54ccd58b49dc43500b635.png', '1', null, null, '2016-05-08 06:57:46', '$2y$10$4k/U2v9eIF3iTFAbpbihu.aBZDDIc6yLH.d6suUvOiv814/XWXZpi', null, null, null, '0000-00-00 00:00:00', '2016-05-08 06:57:46');

-- ----------------------------
-- Table structure for `users_groups`
-- ----------------------------
DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE `users_groups` (
  `user_id` int(10) unsigned NOT NULL,
  `group_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`user_id`,`group_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of users_groups
-- ----------------------------
INSERT INTO `users_groups` VALUES ('1', '1');
INSERT INTO `users_groups` VALUES ('2', '1');
INSERT INTO `users_groups` VALUES ('3', '3');
INSERT INTO `users_groups` VALUES ('6', '4');
INSERT INTO `users_groups` VALUES ('7', '5');
INSERT INTO `users_groups` VALUES ('8', '2');
INSERT INTO `users_groups` VALUES ('9', '1');
