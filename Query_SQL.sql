# Tridharma OLTP
SELECT pengajuan.*
    ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='A',detail_pengajuan.Nilai_asli,0)) as A
    ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='B',detail_pengajuan.Nilai_asli,0)) as B
    ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='C',detail_pengajuan.Nilai_asli,0)) as C
    ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='D',detail_pengajuan.Nilai_asli,0)) as D
    ,nama_dosen,m_dosen.Kode_Jurusan,m_dosen.Kode_Fakultas,Nama_Fakultas,Nama_Jurusan,Tahun
    ,Semester,Nilai_Kum_Akhir,jabatan_akademik,m_dosen.Id_Jabatan,jabatan_akademik.nilai as nilai_jabatan_akademik_awal
FROM pengajuan
LEFT JOIN detail_pengajuan  ON detail_pengajuan.No_Pengajuan = pengajuan.No_Pengajuan
LEFT JOIN tahun_pengajuan ON tahun_pengajuan.Kode_Tahun_Pengajuan = pengajuan.Kode_Tahun_Pengajuan
LEFT JOIN m_dosen ON m_dosen.nidn = pengajuan.nidn
LEFT JOIN fakultas ON fakultas.Kode_Fakultas = m_dosen.Kode_Fakultas
LEFT JOIN jurusan ON jurusan.Kode_Jurusan = m_dosen.Kode_Jurusan
LEFT JOIN jabatan_akademik ON jabatan_akademik.id_jabatan =  m_dosen.Id_Jabatan
WHERE pengajuan.Kode_Tahun_Pengajuan = '1'
AND m_dosen.Kode_Jurusan = 'S1TI'
GROUP BY pengajuan.No_Pengajuan


# Tridharma OLAP
SELECT fact_laporan2_3.*
    ,fact_laporan2_3.Kum_Pengajaran as A
    ,fact_laporan2_3.Kum_Penelitian as B
    ,fact_laporan2_3.Kum_PengMasy as C
    ,fact_laporan2_3.Kum_UnsurPenunjang as D
    ,fact_laporan2_3.Nama_Dosen as nama_dosen
    ,dim_jurusanfakultas.Nama_Jurusan
    ,fact_laporan2_3.Jabatan_Akademik as jabatan_akademik
FROM fact_laporan2_3
LEFT JOIN dim_jurusanfakultas ON fact_laporan2_3.Kode_JurusanFakultas=dim_jurusanfakultas.Kode_JurusanFakultas
WHERE fact_laporan2_3.Kode_Tahun_Pengajuan = '1'
AND m_dosen.Kode_Jurusan = 'S1TI'


# Jafung OLTP
SELECT pengajuan.*
    ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='A',detail_pengajuan.Nilai_asli,0)) as A
    ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='B',detail_pengajuan.Nilai_asli,0)) as B
    ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='C',detail_pengajuan.Nilai_asli,0)) as C
    ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='D',detail_pengajuan.Nilai_asli,0)) as D
    ,nama_dosen,m_dosen.Kode_Jurusan,m_dosen.Kode_Fakultas,Nama_Fakultas,Nama_Jurusan,Tahun
    ,Semester,Nilai_Kum_Akhir,jabatan_akademik,m_dosen.Id_Jabatan,jabatan_akademik.nilai as nilai_jabatan_akademik_awal
FROM pengajuan
LEFT JOIN detail_pengajuan  ON detail_pengajuan.No_Pengajuan = pengajuan.No_Pengajuan
LEFT JOIN tahun_pengajuan ON tahun_pengajuan.Kode_Tahun_Pengajuan = pengajuan.Kode_Tahun_Pengajuan
LEFT JOIN m_dosen ON m_dosen.nidn = pengajuan.nidn
LEFT JOIN fakultas ON fakultas.Kode_Fakultas = m_dosen.Kode_Fakultas
LEFT JOIN jurusan ON jurusan.Kode_Jurusan = m_dosen.Kode_Jurusan
LEFT JOIN jabatan_akademik ON jabatan_akademik.id_jabatan =  m_dosen.Id_Jabatan
WHERE pengajuan.Kode_Tahun_Pengajuan = '1'
AND m_dosen.Kode_Jurusan = 'S1TI'
GROUP BY pengajuan.No_Pengajuan

# Jafung OLAP
SELECT fact_laporan1.*
    ,fact_laporan1.Presentase_Kum_Pengajaran as A
    ,fact_laporan1.Persentase_Kum_Penelitian as B
    ,fact_laporan1.Persentase_Kum_PengMasy as C
    ,fact_laporan1.Persentase_Kum_UnsurPenunjang as D
    ,fact_laporan1.Nama_Dosen as nama_dosen                        
    ,fact_laporan1.Jabatan_Akademik as jabatan_akademik
FROM fact_laporan1
LEFT JOIN dim_jurusanfakultas ON fact_laporan1.Kode_JurusanFakultas=dim_jurusanfakultas.Kode_JurusanFakultas
WHERE fact_laporan1.Kode_Tahun_Pengajuan = '1'
AND SUBSTR(fact_laporan1.Kode_JurusanFakultas,1,4) = 'S1TI'



# Jabatan Akademik OLTP
SELECT a.*,b.Nama_Dosen,b.Nilai_Kum_Akhir,c.jabatan_akademik
FROM pengajuan a
LEFT JOIN m_dosen b ON b.NIDN=a.NIDN
LEFT JOIN jabatan_akademik c ON c.id_jabatan=b.Id_Jabatan
WHERE a.NIDN IS NOT NULL
AND b.Kode_Jurusan = 'S1TI'
AND a.Kode_Tahun_Pengajuan = '1'

# Jabatan Akademik OLAP
SELECT a.*
FROM fact_laporan1 a                  
WHERE a.NIDN IS NOT NULL
AND SUBSTR(a.Kode_JurusanFakultas,1,4) = 'S1TI'
AND a.Kode_Tahun_Pengajuan = '1'

