<?php

class DBHelper {

    public static function tableName($code) {
        switch ($code){
            case 'iia' :
                return 'penilaian_pelaksana_pendidikan_iia';
                break;
            case 'iib' :
                return 'penilaian_pelaksana_pendidikan_iib';
                break;
            case 'iic' :
                return 'penilaian_pelaksana_pendidikan_iic';
                break;
            case 'iid' :
                return 'penilaian_pelaksana_pendidikan_iid';
                break;
            case 'iie' :
                return 'penilaian_pelaksana_pendidikan_iie';
                break;
            case 'iif' :
                return 'penilaian_pelaksana_pendidikan_iif';
                break;
            case 'iig' :
                return 'penilaian_pelaksana_pendidikan_iig';
                break;
            case 'iih' :
                return 'penilaian_pelaksana_pendidikan_iih';
                break;
            case 'iii' :
                return 'penilaian_pelaksana_pendidikan_iii';
                break;
            case 'iij' :
                return 'penilaian_pelaksana_pendidikan_iij';
                break;
            case 'iik' :
                return 'penilaian_pelaksana_pendidikan_iik';
                break;
            case 'iil' :
                return 'penilaian_pelaksana_pendidikan_iil';
                break;
            case 'iim' :
                return 'penilaian_pelaksana_pendidikan_iim';
                break;
            case 'b1a' :
                return 'penilaian_penelitian_b1a';
                break;
            case 'b1b' :
                return 'penilaian_penelitian_b1b';
                break;
            case 'b1c' :
                return 'penilaian_penelitian_b1c';
                break;
            case 'b2a' :
                return 'penilaian_penelitian_b2a';
                break;
            case 'b2b' :
                return 'penilaian_penelitian_b2b';
                break;
            case 'b2c' :
                return 'penilaian_penelitian_b2c';
                break;
            case 'b2d' :
                return 'penilaian_penelitian_b2d';
                break;
            case 'b2e' :
                return 'penilaian_penelitian_b2e';
                break;
            case 'b3' :
                return 'penilaian_penelitian_b3';
                break;
            case 'b4' :
                return 'penilaian_penelitian_b4';
                break;
            case 'b5' :
                return 'penilaian_penelitian_b5';
                break;
            case 'b6' :
                return 'penilaian_penelitian_b6';
                break;
            case 'b7' :
                return 'penilaian_penelitian_b7';
                break;
            case 'b8' :
                return 'penilaian_penelitian_b8';
                break;
            case 'c1' :
                return 'penilaian_pengmasy_c1';
                break;
            case 'c2' :
                return 'penilaian_pengmasy_c2';
                break;
            case 'c3' :
                return 'penilaian_pengmasy_c3';
                break;
            case 'c4' :
                return 'penilaian_pengmasy_c4';
                break;
            case 'c5' :
                return 'penilaian_pengmasy_c5';
                break;
            case 'd1' :
                return 'penilaian_unspenunjang_d1';
                break;
            case 'd2' :
                return 'penilaian_unspenunjang_d2';
                break;
            case 'd3' :
                return 'penilaian_unspenunjang_d3';
                break;
            case 'd4' :
                return 'penilaian_unspenunjang_d4';
                break;
            case 'd5' :
                return 'penilaian_unspenunjang_d5';
                break;
            case 'd6' :
                return 'penilaian_unspenunjang_d6';
                break;
            case 'd7' :
                return 'penilaian_unspenunjang_d7';
                break;
            case 'd8' :
                return 'penilaian_unspenunjang_d8';
                break;
            case 'd9' :
                return 'penilaian_unspenunjang_d9';
                break;
            case 'd10' :
                return 'penilaian_unspenunjang_d10';
                break;
            case 'd11' :
                return 'penilaian_unspenunjang_d11';
                break;
            default :
                return "";
        }
            
    }
    
    
    public static function tableMaster($code) {
        switch ($code){
            case 'iia' :
                return 'm_pelaksanaan_pendidikan';
                break;
            case 'iib' :
                return 'm_pelaksanaan_pendidikan';
                break;
            case 'iic' :
                return 'm_pelaksanaan_pendidikan';
                break;
            case 'iid' :
                return 'm_pelaksanaan_pendidikan';
                break;
            case 'iie' :
                return 'm_pelaksanaan_pendidikan';
                break;
            case 'iif' :
                return 'm_pelaksanaan_pendidikan';
                break;
            case 'iig' :
                return 'm_pelaksanaan_pendidikan';
                break;
            case 'iih' :
                return 'm_pelaksanaan_pendidikan';
                break;
            case 'iii' :
                return 'm_pelaksanaan_pendidikan';
                break;
            case 'iij' :
                return 'm_pelaksanaan_pendidikan';
                break;
            case 'iik' :
                return 'm_pelaksanaan_pendidikan';
                break;
            case 'iil' :
                return 'm_pelaksanaan_pendidikan';
                break;
            case 'iim' :
                return 'm_pelaksanaan_pendidikan';
                break;
            case 'b1a' :
                return 'm_penelitian';
                break;
            case 'b1b' :
                return 'm_penelitian';
                break;
            case 'b1c' :
                return 'm_penelitian';
                break;
            case 'b2a' :
                return 'm_penelitian';
                break;
            case 'b2b' :
                return 'm_penelitian';
                break;
            case 'b2c' :
                return 'm_penelitian';
                break;
            case 'b2d' :
                return 'm_penelitian';
                break;
            case 'b2e' :
                return 'm_penelitian';
                break;
            case 'b3' :
                return 'm_penelitian';
                break;
            case 'b4' :
                return 'm_penelitian';
                break;
            case 'b5' :
                return 'm_penelitian';
                break;
            case 'b6' :
                return 'm_penelitian';
                break;
            case 'b7' :
                return 'm_penelitian';
                break;
            case 'b8' :
                return 'm_penelitian';
                break;
            case 'c1' :
                return 'm_pengmasy';
                break;
            case 'c2' :
                return 'm_pengmasy';
                break;
            case 'c3' :
                return 'm_pengmasy';
                break;
            case 'c4' :
                return 'm_pengmasy';
                break;
            case 'c5' :
                return 'm_pengmasy';
                break;
            case 'd1' :
                return 'm_unsurpenunjang';
                break;
            case 'd2' :
                return 'm_unsurpenunjang';
                break;
            case 'd3' :
                return 'm_unsurpenunjang';
                break;
            case 'd4' :
                return 'm_unsurpenunjang';
                break;
            case 'd5' :
                return 'm_unsurpenunjang';
                break;
            case 'd6' :
                return 'm_unsurpenunjang';
                break;
            case 'd7' :
                return 'm_unsurpenunjang';
                break;
            case 'd8' :
                return 'm_unsurpenunjang';
                break;
            case 'd9' :
                return 'm_unsurpenunjang';
                break;
            case 'd10' :
                return 'm_unsurpenunjang';
                break;
            case 'd11' :
                return 'm_unsurpenunjang';
                break;
            default :
                return "";
        }
            
    }
    
        public static function viewCode($code) {
        switch ($code){
            case 'iia' :
                return 'a';
                break;
            case 'iib' :
                return 'a';
                break;
            case 'iic' :
                return 'a';
                break;
            case 'iid' :
                return 'a';
                break;
            case 'iie' :
                return 'a';
                break;
            case 'iif' :
                return 'a';
                break;
            case 'iig' :
                return 'a';
                break;
            case 'iih' :
                return 'a';
                break;
            case 'iii' :
                return 'a';
                break;
            case 'iij' :
                return 'a';
                break;
            case 'iik' :
                return 'a';
                break;
            case 'iil' :
                return 'a';
                break;
            case 'iim' :
                return 'a';
                break;
            case 'b1a' :
                return 'b1';
                break;
            case 'b1b' :
                return 'b';
                break;
            case 'b1c' :
                return 'b';
                break;
            case 'b2a' :
                return 'b';
                break;
            case 'b2b' :
                return 'b';
                break;
            case 'b2c' :
                return 'b';
                break;
            case 'b2d' :
                return 'b';
                break;
            case 'b2e' :
                return 'b';
                break;
            case 'b3' :
                return 'b';
                break;
            case 'b4' :
                return 'b';
                break;
            case 'b5' :
                return 'b';
                break;
            case 'b6' :
                return 'b';
                break;
            case 'b7' :
                return 'b';
                break;
            case 'b8' :
                return 'b';
                break;
            case 'c1' :
                return 'c';
                break;
            case 'c2' :
                return 'c';
                break;
            case 'c3' :
                return 'c';
                break;
            case 'c4' :
                return 'c';
                break;
            case 'c5' :
                return 'c';
                break;
            case 'd1' :
                return 'd';
                break;
            case 'd2' :
                return 'd';
                break;
            case 'd3' :
                return 'd';
                break;
            case 'd4' :
                return 'd';
                break;
            case 'd5' :
                return 'd';
                break;
            case 'd6' :
                return 'd';
                break;
            case 'd7' :
                return 'd';
                break;
            case 'd8' :
                return 'd';
                break;
            case 'd9' :
                return 'd';
                break;
            case 'd10' :
                return 'd';
                break;
            case 'd11' :
                return 'd';
                break;
            default :
                return "";
        }
            
    }

}
