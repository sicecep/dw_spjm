<?php

class TDHelper {

    public static function getJabatanUsulan($jabatan_akhir) {
        $data = array();
        $jabatan = $jabatan_akhir + 1;
        $m_jabatan = Jabatan::where('id_jabatan','=',$jabatan)->first();
        $id_jabatan = '';
        $nama_jabatan = '';
        $nilai_jabatan = 0;
        if(count($m_jabatan)==1){
            $id_jabatan = $m_jabatan->id_jabatan;
            $nama_jabatan  = $m_jabatan->jabatan_akademik;
            $nilai_jabatan = $m_jabatan->nilai;
        } else {
            $m_jabatan = Jabatan::where('id_jabatan','=',$jabatan_akhir)->first();
            if(count($m_jabatan)==1){
                $id_jabatan = $m_jabatan->id_jabatan;
                $nama_jabatan = $m_jabatan->jabatan_akademik;
                $nilai_jabatan = $m_jabatan->nilai;
            }
        }
        $data['id_jabatan'] = $id_jabatan;
        $data['nama_jabatan'] = $nama_jabatan;
        $data['nilai_jabatan'] = $nilai_jabatan;
        return json_encode($data);
    }
    
    public static function getNilaiIjazah($nidn) {
        $data_pendidikan = RiwayatPendidikan::where('NIDN', '=', $nidn)->orderBy('Pendidikan', 'DESC')->first();
        return isset($data_pendidikan->Angka_Kredit) ? $data_pendidikan->Angka_Kredit : 0;
    }

    public static function getPendidikanTerakhir($nidn) {
        $data_pendidikan = RiwayatPendidikan::where('NIDN', '=', $nidn)->orderBy('Pendidikan', 'DESC')->first();
        return isset($data_pendidikan->Pendidikan) ? $data_pendidikan->Pendidikan : "";
    }
    
    public static function getAngkaKreditMin($id_jabatan_usulan, $selisih_nilai){
        $persentase_jabatan = PersentaseJabatan::where('Kode_Angka_Kredit','=',$id_jabatan_usulan)->first();
        $persentase_pendidikan = $persentase_jabatan->Persentase_Pendidikan * $selisih_nilai;
        $persentase_penelitian = $persentase_jabatan->Persentase_Penelitian * $selisih_nilai;
        $persentase_pengmasy = $persentase_jabatan->Persentase_Pengmasy * $selisih_nilai;
        $persentase_unsur = $persentase_jabatan->Persentase_unsurPenunjang * $selisih_nilai;
        $data = array();
        array_push($data, $persentase_pendidikan);
        array_push($data, $persentase_penelitian);
        array_push($data, $persentase_pengmasy);
        array_push($data, $persentase_unsur);
        return json_encode($data);
    }
    
    public static function getPersentaseAkhir($nilai_asli,$nilai_min,$persen){
//        $nilai_persentase = round(($nilai_asli * $nilai_min)/100);
        $nilai_persentase = round((($persen * $nilai_asli) / $nilai_min) * 100);
        return $nilai_persentase;
    }

}
