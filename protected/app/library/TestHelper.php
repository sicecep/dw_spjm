<?php

class TestHelper {

    public static function getJabatanUsulan($jabatan_akhir) {
        $data = array();
        $jabatan = $jabatan_akhir + 1;
        $m_jabatan = Jabatan::where('id_jabatan', '=', $jabatan)->first();
        $id_jabatan = '';
        $nama_jabatan = '';
        $nilai_jabatan = 0;
        if (count($m_jabatan) == 1) {
            $id_jabatan = $m_jabatan->id_jabatan;
            $nama_jabatan = $m_jabatan->jabatan_akademik;
            $nilai_jabatan = $m_jabatan->nilai;
        } else {
            $m_jabatan = Jabatan::where('id_jabatan', '=', $jabatan_akhir)->first();
            if (count($m_jabatan) == 1) {
                $id_jabatan = $m_jabatan->id_jabatan;
                $nama_jabatan = $m_jabatan->jabatan_akademik;
                $nilai_jabatan = $m_jabatan->nilai;
            }
        }
        $data['id_jabatan'] = $id_jabatan;
        $data['nama_jabatan'] = $nama_jabatan;
        $data['nilai_jabatan'] = $nilai_jabatan;
        return json_encode($data);
    }

}
