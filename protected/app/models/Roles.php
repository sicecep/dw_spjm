<?php

class Roles extends Eloquent{
    protected $table = 'roles';    
    
    public function menu(){
        return $this->belongsTo('Menu','menu_id');
    }
}