<?php
class JurusanFakultas extends Eloquent {
    protected $table = 'dim_jurusanfakultas';
    protected $primaryKey = 'Kode_JurusanFakultas';
    protected $timeStamps = false;
}