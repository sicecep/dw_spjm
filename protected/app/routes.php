<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('uses' => 'MainController@index', 'as' => 'index'));
Route::get('/login', array('uses' => 'MainController@login', 'as' => 'login'));
Route::post('/login', array('uses' => 'MainController@storeLogin', 'as' => 'login'));
Route::get('/register', array('uses' => 'MainController@register', 'as' => 'register'));
Route::get('/logout', array('uses' => 'MainController@getLogout', 'as' => 'users.logout'));
Route::get('/tesRegister', array('uses' => 'MainController@tesRegister', 'as' => 'tesregister'));

Route::get('/daftar-pengajuan', array('uses' => 'PengajuanController@index', 'as' => 'pengajuan.index'));
Route::get('/buat-pengajuan', array('uses' => 'PengajuanController@create', 'as' => 'pengajuan.create'));
Route::post('/buat-pengajuan-2', array('uses' => 'PengajuanController@create2', 'as' => 'pengajuan.create2'));
Route::get('/pengajuan-detail', array('uses' => 'PengajuanController@detail', 'as' => 'pengajuan.detail'));
Route::get('/jabatan_usulan/{ID}', array('uses' => 'PengajuanController@jabatanUsulan', 'as' => 'pengajuan.jabatan_usulan'));

Route::group(array('prefix' => 'dosen/'), function()
{
    Route::any('/', array('uses' => 'DosenController@index', 'as' => 'dosen.index'));
    Route::post('/list', array('uses' => 'DosenController@listDosen', 'as' => 'dosen.list'));
    Route::get('/delete', array('uses' => 'DosenController@deleteDosen', 'as' => 'dosen.delete'));
    Route::get('/edit/{ID}', array('uses' => 'DosenController@editDosen', 'as' => 'dosen.edit'));
    Route::get('/create', array('uses' => 'DosenController@create', 'as' => 'dosen.create'));
    Route::post('/store', array('uses' => 'DosenController@store', 'as' => 'dosen.store'));
    Route::get('/unsur_pendidikan', array('uses' => 'DosenController@unsurPendidikan', 'as' => 'dosen.unsurPendidikan'));
    Route::get('/jurusan/{id}', array('uses' => 'DosenController@getJurusan', 'as' => 'dosen.jurusan'));
});

Route::group(array('prefix' => 'pengajaran/'), function()
{
    Route::get('/', array('uses' => 'PengajaranController@index', 'as' => 'pengajaran.index'));
    Route::get('/create', array('uses' => 'PengajaranController@index', 'as' => 'pengajaran.create'));
    Route::post('/store', array('uses' => 'PengajaranController@index', 'as' => 'pengajaran.store'));    
    Route::get('/unsur_pendidikan', array('uses' => 'PengajaranController@index', 'as' => 'pengajaran.unsurPendidikan'));
    Route::get('/view/{id}', array('uses' => 'PengajaranController@viewFile', 'as' => 'pengajaran.viewFile'));
    
    Route::group(array('prefix' => '2A/'), function() {
        Route::get('/', array('uses' => 'PengajaranController@Pengajaran2A', 'as' => 'pengajaran.2A'));
        Route::any('/list', array('uses' => 'PengajaranController@Pengajaran2AList', 'as' => 'pengajaran.2AList'));
        Route::post('/store', array('uses' => 'PengajaranController@Pengajaran2AStore', 'as' => 'pengajaran.2A.store'));
        Route::get('/delete', array('uses' => 'PengajaranController@deleteData2A', 'as' => 'pengajaran.delete2A'));
        Route::get('/edit/{id}', array('uses' => 'PengajaranController@editData2A', 'as' => 'pengajaran.edit2A'));
    });
    
    Route::group(array('prefix' => '2B/'), function() {
        Route::get('/', array('uses' => 'PengajaranController@Pengajaran2B', 'as' => 'pengajaran.2B'));
        Route::any('/list', array('uses' => 'PengajaranController@Pengajaran2BList', 'as' => 'pengajaran.2BList'));
        Route::post('/store', array('uses' => 'PengajaranController@Pengajaran2BStore', 'as' => 'pengajaran.2B.store'));
        Route::get('/delete', array('uses' => 'PengajaranController@deleteData2B', 'as' => 'pengajaran.delete2B'));
        Route::get('/edit/{id}', array('uses' => 'PengajaranController@editData2B', 'as' => 'pengajaran.edit2B'));
    });
    
    Route::group(array('prefix' => '2C/'), function() {
        Route::get('/', array('uses' => 'PengajaranController@Pengajaran2C', 'as' => 'pengajaran.2C'));
        Route::any('/list', array('uses' => 'PengajaranController@Pengajaran2CList', 'as' => 'pengajaran.2CList'));
        Route::post('/store', array('uses' => 'PengajaranController@Pengajaran2CStore', 'as' => 'pengajaran.2C.store'));
        Route::get('/delete', array('uses' => 'PengajaranController@deleteData2C', 'as' => 'pengajaran.delete2C'));
        Route::get('/edit/{id}', array('uses' => 'PengajaranController@editData2C', 'as' => 'pengajaran.edit2C'));
    });
    
    Route::group(array('prefix' => '2D/'), function() {
        Route::get('/', array('uses' => 'PengajaranController@Pengajaran2D', 'as' => 'pengajaran.2D'));
        Route::any('/list', array('uses' => 'PengajaranController@Pengajaran2DList', 'as' => 'pengajaran.2DList'));
        Route::post('/store', array('uses' => 'PengajaranController@Pengajaran2DStore', 'as' => 'pengajaran.2D.store'));
        Route::get('/delete', array('uses' => 'PengajaranController@deleteData2D', 'as' => 'pengajaran.delete2D'));
        Route::get('/edit/{id}', array('uses' => 'PengajaranController@editData2D', 'as' => 'pengajaran.edit2D'));
    });
    
    Route::group(array('prefix' => '2E/'), function() {
        Route::get('/', array('uses' => 'PengajaranController@Pengajaran2E', 'as' => 'pengajaran.2E'));
        Route::any('/list', array('uses' => 'PengajaranController@Pengajaran2EList', 'as' => 'pengajaran.2EList'));
        Route::post('/store', array('uses' => 'PengajaranController@Pengajaran2EStore', 'as' => 'pengajaran.2E.store'));
        Route::get('/delete', array('uses' => 'PengajaranController@deleteData2E', 'as' => 'pengajaran.delete2E'));
        Route::get('/edit/{id}', array('uses' => 'PengajaranController@editData2E', 'as' => 'pengajaran.edit2E'));
    });
    
    Route::group(array('prefix' => '2F/'), function() {
        Route::get('/', array('uses' => 'PengajaranController@Pengajaran2F', 'as' => 'pengajaran.2F'));
        Route::any('/list', array('uses' => 'PengajaranController@Pengajaran2FList', 'as' => 'pengajaran.2FList'));
        Route::post('/store', array('uses' => 'PengajaranController@Pengajaran2FStore', 'as' => 'pengajaran.2F.store'));
        Route::get('/delete', array('uses' => 'PengajaranController@deleteData2F', 'as' => 'pengajaran.delete2F'));
        Route::get('/edit/{id}', array('uses' => 'PengajaranController@editData2F', 'as' => 'pengajaran.edit2F'));
    });
    
    Route::group(array('prefix' => '2G/'), function() {
        Route::get('/', array('uses' => 'PengajaranController@Pengajaran2G', 'as' => 'pengajaran.2G'));
        Route::any('/list', array('uses' => 'PengajaranController@Pengajaran2GList', 'as' => 'pengajaran.2GList'));
        Route::post('/store', array('uses' => 'PengajaranController@Pengajaran2GStore', 'as' => 'pengajaran.2G.store'));
        Route::get('/delete', array('uses' => 'PengajaranController@deleteData2G', 'as' => 'pengajaran.delete2G'));
        Route::get('/edit/{id}', array('uses' => 'PengajaranController@editData2G', 'as' => 'pengajaran.edit2G'));
    });
    
    Route::group(array('prefix' => '2H/'), function() {
        Route::get('/', array('uses' => 'PengajaranController@Pengajaran2H', 'as' => 'pengajaran.2H'));
        Route::any('/list', array('uses' => 'PengajaranController@Pengajaran2HList', 'as' => 'pengajaran.2HList'));
        Route::post('/store', array('uses' => 'PengajaranController@Pengajaran2HStore', 'as' => 'pengajaran.2H.store'));
        Route::get('/delete', array('uses' => 'PengajaranController@deleteData2H', 'as' => 'pengajaran.delete2H'));
        Route::get('/edit/{id}', array('uses' => 'PengajaranController@editData2H', 'as' => 'pengajaran.edit2H'));
    });
    
    Route::group(array('prefix' => '2I/'), function() {
        Route::get('/', array('uses' => 'PengajaranController@Pengajaran2I', 'as' => 'pengajaran.2I'));
        Route::any('/list', array('uses' => 'PengajaranController@Pengajaran2IList', 'as' => 'pengajaran.2IList'));
        Route::post('/store', array('uses' => 'PengajaranController@Pengajaran2IStore', 'as' => 'pengajaran.2I.store'));
        Route::get('/delete', array('uses' => 'PengajaranController@deleteData2I', 'as' => 'pengajaran.delete2I'));
        Route::get('/edit/{id}', array('uses' => 'PengajaranController@editData2I', 'as' => 'pengajaran.edit2I'));
    });
    
    Route::group(array('prefix' => '2J/'), function() {
        Route::get('/', array('uses' => 'PengajaranController@Pengajaran2J', 'as' => 'pengajaran.2J'));
        Route::any('/list', array('uses' => 'PengajaranController@Pengajaran2JList', 'as' => 'pengajaran.2JList'));
        Route::post('/store', array('uses' => 'PengajaranController@Pengajaran2JStore', 'as' => 'pengajaran.2J.store'));
        Route::get('/delete', array('uses' => 'PengajaranController@deleteData2J', 'as' => 'pengajaran.delete2J'));
        Route::get('/edit/{id}', array('uses' => 'PengajaranController@editData2J', 'as' => 'pengajaran.edit2J'));
    });
    
    Route::group(array('prefix' => '2K/'), function() {
        Route::get('/', array('uses' => 'PengajaranController@Pengajaran2K', 'as' => 'pengajaran.2K'));
        Route::any('/list', array('uses' => 'PengajaranController@Pengajaran2KList', 'as' => 'pengajaran.2KList'));
        Route::post('/store', array('uses' => 'PengajaranController@Pengajaran2KStore', 'as' => 'pengajaran.2K.store'));
        Route::get('/delete', array('uses' => 'PengajaranController@deleteData2K', 'as' => 'pengajaran.delete2K'));
        Route::get('/edit/{id}', array('uses' => 'PengajaranController@editData2K', 'as' => 'pengajaran.edit2K'));
    });
    
    Route::group(array('prefix' => '2L/'), function() {
        Route::get('/', array('uses' => 'PengajaranController@Pengajaran2L', 'as' => 'pengajaran.2L'));
        Route::any('/list', array('uses' => 'PengajaranController@Pengajaran2LList', 'as' => 'pengajaran.2LList'));
        Route::post('/store', array('uses' => 'PengajaranController@Pengajaran2LStore', 'as' => 'pengajaran.2L.store'));
        Route::get('/delete', array('uses' => 'PengajaranController@deleteData2L', 'as' => 'pengajaran.delete2L'));
        Route::get('/edit/{id}', array('uses' => 'PengajaranController@editData2L', 'as' => 'pengajaran.edit2L'));
    });
    
    Route::group(array('prefix' => '2M/'), function() {
        Route::get('/', array('uses' => 'PengajaranController@Pengajaran2M', 'as' => 'pengajaran.2M'));
        Route::any('/list', array('uses' => 'PengajaranController@Pengajaran2MList', 'as' => 'pengajaran.2MList'));
        Route::post('/store', array('uses' => 'PengajaranController@Pengajaran2MStore', 'as' => 'pengajaran.2M.store'));
        Route::get('/delete', array('uses' => 'PengajaranController@deleteData2M', 'as' => 'pengajaran.delete2M'));
        Route::get('/edit/{id}', array('uses' => 'PengajaranController@editData2M', 'as' => 'pengajaran.edit2M'));
    });
});

Route::group(array('prefix' => 'penelitian/'), function()
{
    Route::get('/', array('uses' => 'PenelitianController@index', 'as' => 'penelitian.index'));
    Route::get('/create', array('uses' => 'PenelitianController@index', 'as' => 'penelitian.create'));
    Route::post('/store', array('uses' => 'PenelitianController@index', 'as' => 'penelitian.store'));
    Route::get('/unsur_pendidikan', array('uses' => 'PenelitianController@index', 'as' => 'penelitian.unsurPendidikan'));
    Route::get('/view/{id}', array('uses' => 'PenelitianController@viewFile', 'as' => 'penelitian.viewFile'));
    
    Route::group(array('prefix' => 'B1A/'), function() {
        Route::get('/', array('uses' => 'PenelitianController@PenelitianB1A', 'as' => 'penelitian.B1A'));
        Route::any('/list', array('uses' => 'PenelitianController@PenelitianB1AList', 'as' => 'penelitian.B1AList'));
        Route::post('/store', array('uses' => 'PenelitianController@PenelitianB1AStore', 'as' => 'penelitian.B1A.store'));
        Route::get('/delete', array('uses' => 'PenelitianController@deleteDataB1A', 'as' => 'penelitian.deleteB1A'));
        Route::get('/edit/{id}', array('uses' => 'PenelitianController@editDataB1A', 'as' => 'penelitian.editB1A'));
    });
    
    Route::group(array('prefix' => 'B1B/'), function() {
        Route::get('/', array('uses' => 'PenelitianController@PenelitianB1B', 'as' => 'penelitian.B1B'));
        Route::any('/list', array('uses' => 'PenelitianController@PenelitianB1BList', 'as' => 'penelitian.B1BList'));
        Route::post('/store', array('uses' => 'PenelitianController@PenelitianB1BStore', 'as' => 'penelitian.B1B.store'));
        Route::get('/delete', array('uses' => 'PenelitianController@deleteDataB1B', 'as' => 'penelitian.deleteB1B'));
        Route::get('/edit/{id}', array('uses' => 'PenelitianController@editDataB1B', 'as' => 'penelitian.editB1B'));
    });
    
    Route::group(array('prefix' => 'B1C/'), function() {
        Route::get('/', array('uses' => 'PenelitianController@PenelitianB1C', 'as' => 'penelitian.B1C'));
        Route::any('/list', array('uses' => 'PenelitianController@PenelitianB1CList', 'as' => 'penelitian.B1CList'));
        Route::post('/store', array('uses' => 'PenelitianController@PenelitianB1CStore', 'as' => 'penelitian.B1C.store'));
        Route::get('/delete', array('uses' => 'PenelitianController@deleteDataB1C', 'as' => 'penelitian.deleteB1C'));
        Route::get('/edit/{id}', array('uses' => 'PenelitianController@editDataB1C', 'as' => 'penelitian.editB1C'));
    });
    
    Route::group(array('prefix' => 'B2A/'), function() {
        Route::get('/', array('uses' => 'PenelitianController@PenelitianB2A', 'as' => 'penelitian.B2A'));
        Route::any('/list', array('uses' => 'PenelitianController@PenelitianB2AList', 'as' => 'penelitian.B2AList'));
        Route::post('/store', array('uses' => 'PenelitianController@PenelitianB2AStore', 'as' => 'penelitian.B2A.store'));
        Route::get('/delete', array('uses' => 'PenelitianController@deleteDataB2A', 'as' => 'penelitian.deleteB2A'));
        Route::get('/edit/{id}', array('uses' => 'PenelitianController@editDataB2A', 'as' => 'penelitian.editB2A'));
    });
    
    Route::group(array('prefix' => 'B2B/'), function() {
        Route::get('/', array('uses' => 'PenelitianController@PenelitianB2B', 'as' => 'penelitian.B2B'));
        Route::any('/list', array('uses' => 'PenelitianController@PenelitianB2BList', 'as' => 'penelitian.B2BList'));
        Route::post('/store', array('uses' => 'PenelitianController@PenelitianB2BStore', 'as' => 'penelitian.B2B.store'));
        Route::get('/delete', array('uses' => 'PenelitianController@deleteDataB2B', 'as' => 'penelitian.deleteB2B'));
        Route::get('/edit/{id}', array('uses' => 'PenelitianController@editDataB2B', 'as' => 'penelitian.editB2B'));
    });
    
    Route::group(array('prefix' => 'B2C/'), function() {
        Route::get('/', array('uses' => 'PenelitianController@PenelitianB2C', 'as' => 'penelitian.B2C'));
        Route::any('/list', array('uses' => 'PenelitianController@PenelitianB2CList', 'as' => 'penelitian.B2CList'));
        Route::post('/store', array('uses' => 'PenelitianController@PenelitianB2CStore', 'as' => 'penelitian.B2C.store'));
        Route::get('/delete', array('uses' => 'PenelitianController@deleteDataB2C', 'as' => 'penelitian.deleteB2C'));
        Route::get('/edit/{id}', array('uses' => 'PenelitianController@editDataB2C', 'as' => 'penelitian.editB2C'));
    });
    
     Route::group(array('prefix' => 'B2D/'), function() {
        Route::get('/', array('uses' => 'PenelitianController@PenelitianB2D', 'as' => 'penelitian.B2D'));
        Route::any('/list', array('uses' => 'PenelitianController@PenelitianB2DList', 'as' => 'penelitian.B2DList'));
        Route::post('/store', array('uses' => 'PenelitianController@PenelitianB2DStore', 'as' => 'penelitian.B2D.store'));
        Route::get('/delete', array('uses' => 'PenelitianController@deleteDataB2D', 'as' => 'penelitian.deleteB2D'));
        Route::get('/edit/{id}', array('uses' => 'PenelitianController@editDataB2D', 'as' => 'penelitian.editB2D'));
    });
    
     Route::group(array('prefix' => 'B2E/'), function() {
        Route::get('/', array('uses' => 'PenelitianController@PenelitianB2E', 'as' => 'penelitian.B2E'));
        Route::any('/list', array('uses' => 'PenelitianController@PenelitianB2EList', 'as' => 'penelitian.B2EList'));
        Route::post('/store', array('uses' => 'PenelitianController@PenelitianB2EStore', 'as' => 'penelitian.B2E.store'));
        Route::get('/delete', array('uses' => 'PenelitianController@deleteDataB2E', 'as' => 'penelitian.deleteB2E'));
        Route::get('/edit/{id}', array('uses' => 'PenelitianController@editDataB2E', 'as' => 'penelitian.editB2E'));
    });
    
    Route::group(array('prefix' => 'B3/'), function() {
        Route::get('/', array('uses' => 'PenelitianController@PenelitianB3', 'as' => 'penelitian.B3'));
        Route::any('/list', array('uses' => 'PenelitianController@PenelitianB3List', 'as' => 'penelitian.B3List'));
        Route::post('/store', array('uses' => 'PenelitianController@PenelitianB3Store', 'as' => 'penelitian.B3.store'));
        Route::get('/delete', array('uses' => 'PenelitianController@deleteDataB3', 'as' => 'penelitian.deleteB3'));
        Route::get('/edit/{id}', array('uses' => 'PenelitianController@editDataB3', 'as' => 'penelitian.editB3'));
    });
    
    Route::group(array('prefix' => 'B4/'), function() {
        Route::get('/', array('uses' => 'PenelitianController@PenelitianB4', 'as' => 'penelitian.B4'));
        Route::any('/list', array('uses' => 'PenelitianController@PenelitianB4List', 'as' => 'penelitian.B4List'));
        Route::post('/store', array('uses' => 'PenelitianController@PenelitianB4Store', 'as' => 'penelitian.B4.store'));
        Route::get('/delete', array('uses' => 'PenelitianController@deleteDataB4', 'as' => 'penelitian.deleteB4'));
        Route::get('/edit/{id}', array('uses' => 'PenelitianController@editDataB4', 'as' => 'penelitian.editB4'));
    });
    
    Route::group(array('prefix' => 'B5/'), function() {
        Route::get('/', array('uses' => 'PenelitianController@PenelitianB5', 'as' => 'penelitian.B5'));
        Route::any('/list', array('uses' => 'PenelitianController@PenelitianB5List', 'as' => 'penelitian.B5List'));
        Route::post('/store', array('uses' => 'PenelitianController@PenelitianB5Store', 'as' => 'penelitian.B5.store'));
        Route::get('/delete', array('uses' => 'PenelitianController@deleteDataB5', 'as' => 'penelitian.deleteB5'));
        Route::get('/edit/{id}', array('uses' => 'PenelitianController@editDataB5', 'as' => 'penelitian.editB5'));
    });
    
    Route::group(array('prefix' => 'B6/'), function() {
        Route::get('/', array('uses' => 'PenelitianController@PenelitianB6', 'as' => 'penelitian.B6'));
        Route::any('/list', array('uses' => 'PenelitianController@PenelitianB6List', 'as' => 'penelitian.B6List'));
        Route::post('/store', array('uses' => 'PenelitianController@PenelitianB6Store', 'as' => 'penelitian.B6.store'));
        Route::get('/delete', array('uses' => 'PenelitianController@deleteDataB6', 'as' => 'penelitian.deleteB6'));
        Route::get('/edit/{id}', array('uses' => 'PenelitianController@editDataB6', 'as' => 'penelitian.editB6'));
    });
    
    Route::group(array('prefix' => 'B7/'), function() {
        Route::get('/', array('uses' => 'PenelitianController@PenelitianB7', 'as' => 'penelitian.B7'));
        Route::any('/list', array('uses' => 'PenelitianController@PenelitianB7List', 'as' => 'penelitian.B7List'));
        Route::post('/store', array('uses' => 'PenelitianController@PenelitianB7Store', 'as' => 'penelitian.B7.store'));
        Route::get('/delete', array('uses' => 'PenelitianController@deleteDataB7', 'as' => 'penelitian.deleteB7'));
        Route::get('/edit/{id}', array('uses' => 'PenelitianController@editDataB7', 'as' => 'penelitian.editB7'));
    });
    
    Route::group(array('prefix' => 'B8/'), function() {
        Route::get('/', array('uses' => 'PenelitianController@PenelitianB8', 'as' => 'penelitian.B8'));
        Route::any('/list', array('uses' => 'PenelitianController@PenelitianB8List', 'as' => 'penelitian.B8List'));
        Route::post('/store', array('uses' => 'PenelitianController@PenelitianB8Store', 'as' => 'penelitian.B8.store'));
        Route::get('/delete', array('uses' => 'PenelitianController@deleteDataB8', 'as' => 'penelitian.deleteB8'));
        Route::get('/edit/{id}', array('uses' => 'PenelitianController@editDataB8', 'as' => 'penelitian.editB8'));
    });
});

Route::group(array('prefix' => 'pengabdian/'), function()
{
    Route::get('/', array('uses' => 'PengabdianController@index', 'as' => 'pengabdian.index'));
    Route::get('/create', array('uses' => 'PengabdianController@index', 'as' => 'pengabdian.create'));
    Route::post('/store', array('uses' => 'PengabdianController@index', 'as' => 'pengabdian.store'));
    Route::get('/unsur_pendidikan', array('uses' => 'PengabdianController@index', 'as' => 'pengabdian.unsurPendidikan'));
    Route::get('/view/{id}', array('uses' => 'PengabdianController@viewFile', 'as' => 'pengabdian.viewFile'));
    
    Route::group(array('prefix' => 'C1/'), function() {
        Route::get('/', array('uses' => 'PengabdianController@PengabdianC1', 'as' => 'pengabdian.C1'));
        Route::any('/list', array('uses' => 'PengabdianController@PengabdianC1List', 'as' => 'pengabdian.C1List'));
        Route::post('/store', array('uses' => 'PengabdianController@PengabdianC1Store', 'as' => 'pengabdian.C1.store'));
        Route::get('/delete', array('uses' => 'PengabdianController@deleteDataC1', 'as' => 'pengabdian.deleteC1'));
        Route::get('/edit/{id}', array('uses' => 'PengabdianController@editDataC1', 'as' => 'pengabdian.editC1'));
    });
    
    Route::group(array('prefix' => 'C2/'), function() {
        Route::get('/', array('uses' => 'PengabdianController@PengabdianC2', 'as' => 'pengabdian.C2'));
        Route::any('/list', array('uses' => 'PengabdianController@PengabdianC2List', 'as' => 'pengabdian.C2List'));
        Route::post('/store', array('uses' => 'PengabdianController@PengabdianC2Store', 'as' => 'pengabdian.C2.store'));
        Route::get('/delete', array('uses' => 'PengabdianController@deleteDataC2', 'as' => 'pengabdian.deleteC2'));
        Route::get('/edit/{id}', array('uses' => 'PengabdianController@editDataC2', 'as' => 'pengabdian.editC2'));
    });
    
    Route::group(array('prefix' => 'C3/'), function() {
        Route::get('/', array('uses' => 'PengabdianController@PengabdianC3', 'as' => 'pengabdian.C3'));
        Route::any('/list', array('uses' => 'PengabdianController@PengabdianC3List', 'as' => 'pengabdian.C3List'));
        Route::post('/store', array('uses' => 'PengabdianController@PengabdianC3Store', 'as' => 'pengabdian.C3.store'));
        Route::get('/delete', array('uses' => 'PengabdianController@deleteDataC3', 'as' => 'pengabdian.deleteC3'));
        Route::get('/edit/{id}', array('uses' => 'PengabdianController@editDataC3', 'as' => 'pengabdian.editC3'));
    });
    
    Route::group(array('prefix' => 'C4/'), function() {
        Route::get('/', array('uses' => 'PengabdianController@PengabdianC4', 'as' => 'pengabdian.C4'));
        Route::any('/list', array('uses' => 'PengabdianController@PengabdianC4List', 'as' => 'pengabdian.C4List'));
        Route::post('/store', array('uses' => 'PengabdianController@PengabdianC4Store', 'as' => 'pengabdian.C4.store'));
        Route::get('/delete', array('uses' => 'PengabdianController@deleteDataC4', 'as' => 'pengabdian.deleteC4'));
        Route::get('/edit/{id}', array('uses' => 'PengabdianController@editDataC4', 'as' => 'pengabdian.editC4'));
    });
    
    Route::group(array('prefix' => 'C5/'), function() {
        Route::get('/', array('uses' => 'PengabdianController@PengabdianC5', 'as' => 'pengabdian.C5'));
        Route::any('/list', array('uses' => 'PengabdianController@PengabdianC5List', 'as' => 'pengabdian.C5List'));
        Route::post('/store', array('uses' => 'PengabdianController@PengabdianC5Store', 'as' => 'pengabdian.C5.store'));
        Route::get('/delete', array('uses' => 'PengabdianController@deleteDataC5', 'as' => 'pengabdian.deleteC5'));
        Route::get('/edit/{id}', array('uses' => 'PengabdianController@editDataC5', 'as' => 'pengabdian.editC5'));
    });
});

Route::group(array('prefix' => 'unsur_penunjang/'), function()
{
    Route::get('/', array('uses' => 'UnsurPenunjangController@index', 'as' => 'unsur_penunjang.index'));
    Route::get('/create', array('uses' => 'UnsurPenunjangController@index', 'as' => 'unsur_penunjang.create'));
    Route::post('/store', array('uses' => 'UnsurPenunjangController@index', 'as' => 'unsur_penunjang.store'));
    Route::get('/unsur_pendidikan', array('uses' => 'UnsurPenunjangController@index', 'as' => 'unsur_penunjang.unsurPendidikan'));
    Route::get('/view/{id}', array('uses' => 'UnsurPenunjangController@viewFile', 'as' => 'unsur_penunjang.viewFile'));
    
    Route::group(array('prefix' => 'D1/'), function() {
        Route::get('/', array('uses' => 'UnsurPenunjangController@PenunjangD1', 'as' => 'unsur_penunjang.D1'));
        Route::any('/list', array('uses' => 'UnsurPenunjangController@PenunjangD1List', 'as' => 'unsur_penunjang.D1List'));
        Route::post('/store', array('uses' => 'UnsurPenunjangController@PenunjangD1Store', 'as' => 'unsur_penunjang.D1.store'));
        Route::get('/delete', array('uses' => 'UnsurPenunjangController@deleteDataD1', 'as' => 'unsur_penunjang.deleteD1'));
        Route::get('/edit/{id}', array('uses' => 'UnsurPenunjangController@editDataD1', 'as' => 'unsur_penunjang.editD1'));
    });
    
     Route::group(array('prefix' => 'D2/'), function() {
        Route::get('/', array('uses' => 'UnsurPenunjangController@PenunjangD2', 'as' => 'unsur_penunjang.D2'));
        Route::any('/list', array('uses' => 'UnsurPenunjangController@PenunjangD2List', 'as' => 'unsur_penunjang.D2List'));
        Route::post('/store', array('uses' => 'UnsurPenunjangController@PenunjangD2Store', 'as' => 'unsur_penunjang.D2.store'));
        Route::get('/delete', array('uses' => 'UnsurPenunjangController@deleteDataD2', 'as' => 'unsur_penunjang.deleteD2'));
        Route::get('/edit/{id}', array('uses' => 'UnsurPenunjangController@editDataD2', 'as' => 'unsur_penunjang.editD2'));
    });
    
    Route::group(array('prefix' => 'D3/'), function() {
        Route::get('/', array('uses' => 'UnsurPenunjangController@PenunjangD3', 'as' => 'unsur_penunjang.D3'));
        Route::any('/list', array('uses' => 'UnsurPenunjangController@PenunjangD3List', 'as' => 'unsur_penunjang.D3List'));
        Route::post('/store', array('uses' => 'UnsurPenunjangController@PenunjangD3Store', 'as' => 'unsur_penunjang.D3.store'));
        Route::get('/delete', array('uses' => 'UnsurPenunjangController@deleteDataD3', 'as' => 'unsur_penunjang.deleteD3'));
        Route::get('/edit/{id}', array('uses' => 'UnsurPenunjangController@editDataD3', 'as' => 'unsur_penunjang.editD3'));
    });
    
    Route::group(array('prefix' => 'D4/'), function() {
        Route::get('/', array('uses' => 'UnsurPenunjangController@PenunjangD4', 'as' => 'unsur_penunjang.D4'));
        Route::any('/list', array('uses' => 'UnsurPenunjangController@PenunjangD4List', 'as' => 'unsur_penunjang.D4List'));
        Route::post('/store', array('uses' => 'UnsurPenunjangController@PenunjangD4Store', 'as' => 'unsur_penunjang.D4.store'));
        Route::get('/delete', array('uses' => 'UnsurPenunjangController@deleteDataD4', 'as' => 'unsur_penunjang.deleteD4'));
        Route::get('/edit/{id}', array('uses' => 'UnsurPenunjangController@editDataD4', 'as' => 'unsur_penunjang.editD4'));
    });
    
    Route::group(array('prefix' => 'D5/'), function() {
        Route::get('/', array('uses' => 'UnsurPenunjangController@PenunjangD5', 'as' => 'unsur_penunjang.D5'));
        Route::any('/list', array('uses' => 'UnsurPenunjangController@PenunjangD5List', 'as' => 'unsur_penunjang.D5List'));
        Route::post('/store', array('uses' => 'UnsurPenunjangController@PenunjangD5Store', 'as' => 'unsur_penunjang.D5.store'));
        Route::get('/delete', array('uses' => 'UnsurPenunjangController@deleteDataD5', 'as' => 'unsur_penunjang.deleteD5'));
        Route::get('/edit/{id}', array('uses' => 'UnsurPenunjangController@editDataD5', 'as' => 'unsur_penunjang.editD5'));
    });
    
    Route::group(array('prefix' => 'D6/'), function() {
        Route::get('/', array('uses' => 'UnsurPenunjangController@PenunjangD6', 'as' => 'unsur_penunjang.D6'));
        Route::any('/list', array('uses' => 'UnsurPenunjangController@PenunjangD6List', 'as' => 'unsur_penunjang.D6List'));
        Route::post('/store', array('uses' => 'UnsurPenunjangController@PenunjangD6Store', 'as' => 'unsur_penunjang.D6.store'));
        Route::get('/delete', array('uses' => 'UnsurPenunjangController@deleteDataD6', 'as' => 'unsur_penunjang.deleteD6'));
        Route::get('/edit/{id}', array('uses' => 'UnsurPenunjangController@editDataD6', 'as' => 'unsur_penunjang.editD6'));
    });
    
    Route::group(array('prefix' => 'D7/'), function() {
        Route::get('/', array('uses' => 'UnsurPenunjangController@PenunjangD7', 'as' => 'unsur_penunjang.D7'));
        Route::any('/list', array('uses' => 'UnsurPenunjangController@PenunjangD7List', 'as' => 'unsur_penunjang.D7List'));
        Route::post('/store', array('uses' => 'UnsurPenunjangController@PenunjangD7Store', 'as' => 'unsur_penunjang.D7.store'));
        Route::get('/delete', array('uses' => 'UnsurPenunjangController@deleteDataD7', 'as' => 'unsur_penunjang.deleteD7'));
        Route::get('/edit/{id}', array('uses' => 'UnsurPenunjangController@editDataD7', 'as' => 'unsur_penunjang.editD7'));
    });
    
    Route::group(array('prefix' => 'D8/'), function() {
        Route::get('/', array('uses' => 'UnsurPenunjangController@PenunjangD8', 'as' => 'unsur_penunjang.D8'));
        Route::any('/list', array('uses' => 'UnsurPenunjangController@PenunjangD8List', 'as' => 'unsur_penunjang.D8List'));
        Route::post('/store', array('uses' => 'UnsurPenunjangController@PenunjangD8Store', 'as' => 'unsur_penunjang.D8.store'));
        Route::get('/delete', array('uses' => 'UnsurPenunjangController@deleteDataD8', 'as' => 'unsur_penunjang.deleteD8'));
        Route::get('/edit/{id}', array('uses' => 'UnsurPenunjangController@editDataD8', 'as' => 'unsur_penunjang.editD8'));
    });
    
    Route::group(array('prefix' => 'D9/'), function() {
        Route::get('/', array('uses' => 'UnsurPenunjangController@PenunjangD9', 'as' => 'unsur_penunjang.D9'));
        Route::any('/list', array('uses' => 'UnsurPenunjangController@PenunjangD9List', 'as' => 'unsur_penunjang.D9List'));
        Route::post('/store', array('uses' => 'UnsurPenunjangController@PenunjangD9Store', 'as' => 'unsur_penunjang.D9.store'));
        Route::get('/delete', array('uses' => 'UnsurPenunjangController@deleteDataD9', 'as' => 'unsur_penunjang.deleteD9'));
        Route::get('/edit/{id}', array('uses' => 'UnsurPenunjangController@editDataD9', 'as' => 'unsur_penunjang.editD9'));
    });
    
     Route::group(array('prefix' => 'D10/'), function() {
        Route::get('/', array('uses' => 'UnsurPenunjangController@PenunjangD10', 'as' => 'unsur_penunjang.D10'));
        Route::any('/list', array('uses' => 'UnsurPenunjangController@PenunjangD10List', 'as' => 'unsur_penunjang.D10List'));
        Route::post('/store', array('uses' => 'UnsurPenunjangController@PenunjangD10Store', 'as' => 'unsur_penunjang.D10.store'));
        Route::get('/delete', array('uses' => 'UnsurPenunjangController@deleteDataD10', 'as' => 'unsur_penunjang.deleteD10'));
        Route::get('/edit/{id}', array('uses' => 'UnsurPenunjangController@editDataD10', 'as' => 'unsur_penunjang.editD10'));
    });
    
    Route::group(array('prefix' => 'D11/'), function() {
        Route::get('/', array('uses' => 'UnsurPenunjangController@PenunjangD11', 'as' => 'unsur_penunjang.D11'));
        Route::any('/list', array('uses' => 'UnsurPenunjangController@PenunjangD11List', 'as' => 'unsur_penunjang.D11List'));
        Route::post('/store', array('uses' => 'UnsurPenunjangController@PenunjangD11Store', 'as' => 'unsur_penunjang.D11.store'));
        Route::get('/delete', array('uses' => 'UnsurPenunjangController@deleteDataD11', 'as' => 'unsur_penunjang.deleteD11'));
        Route::get('/edit/{id}', array('uses' => 'UnsurPenunjangController@editDataD11', 'as' => 'unsur_penunjang.editD11'));
    });
});

Route::group(array('prefix' => 'informasi/'), function()
{
    Route::get('/', array('uses' => 'InformasiController@index', 'as' => 'informasi.index'));
    Route::get('/create', array('uses' => 'InformasiController@index', 'as' => 'informasi.create'));
    Route::post('/store', array('uses' => 'InformasiController@index', 'as' => 'informasi.store'));
    Route::get('/unsur_pendidikan', array('uses' => 'InformasiController@index', 'as' => 'informasi.unsurPendidikan'));
});


// route dw spjm
Route::group(array('prefix' => 'fetch/'), function()
{
    Route::get('/etl', array('uses' => 'FetchController@fetchData', 'as' => 'fetch.data'));   
});

Route::group(array('prefix' => 'laporan/'), function()
{
    Route::get('/tridharma_oltp', array('uses' => 'LaporanController@LapTridharmaOLTP', 'as' => 'laporan.tridharma.oltp'));
    Route::get('/tridharma_olap', array('uses' => 'LaporanController@LapTridharmaOLAP', 'as' => 'laporan.tridharma.olap'));
    
    Route::get('/jafung_oltp', array('uses' => 'LaporanController@LapJafungOLTP', 'as' => 'laporan.jafung.oltp'));
    Route::get('/jafung_olap', array('uses' => 'LaporanController@LapJafungOLAP', 'as' => 'laporan.jafung.olap'));
    
    Route::get('/jabatan_oltp', array('uses' => 'LaporanController@LapJabatanOLTP', 'as' => 'laporan.jabatan.oltp'));
    Route::get('/jabatan_olap', array('uses' => 'LaporanController@LapJabatanOLAP', 'as' => 'laporan.jabatan.olap'));
    
    Route::get('/tridharma_grafik', array('uses' => 'LaporanController@GrafikTridharma', 'as' => 'laporan.tridharma.grafik'));
    Route::get('/jafung_grafik', array('uses' => 'LaporanController@GrafikJafung', 'as' => 'laporan.jafung.grafik'));
    Route::get('/jabatan_grafik', array('uses' => 'LaporanController@GrafikJabatan', 'as' => 'laporan.jabatan.grafik'));
    
    Route::get('/lap_sql', array('uses' => 'LaporanController@SQLSyntax', 'as' => 'laporan.sql'));
});