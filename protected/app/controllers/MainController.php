<?php

class MainController extends BaseController {
//    protected $layout = 'backend.index';
    public function index() {
        if(!Sentry::check()){
            return Redirect::route('login');
        } else {
//            $user = Sentry::getUser();
//            $group = Sentry::findGroupById($user['id']);
//            $groupPermissions = $group->getPermissions();           
//            $menu = Menu::where('active','=','1')->get();
//            $roles = $this->setMenuParent($group->id,0);
            $view = View::make('dashboard');
            $this->layout = $view;
        }
    }
    
    public function login() {
        if (!Sentry::check()) {
            return View::make('login');
        } else {
            return Redirect::route('index');
        }
    }
    
    public function register(){
        Sentry::register(array(
            'email' => 'sepry@kudo.co.id',
            'password' => '123456'
        ));
    }
    
    public function storeLogin(){
        $inputs = array('identity'=>Input::get('identity'),'password'=>Input::get('password'));
        
        $rules = array(
            'identity' => 'required|min:4|max:32',
            'password' => 'required|min:6'
        );
        
        // find is that username or email
        if(filter_var(Input::get('identity'),FILTER_VALIDATE_EMAIL))
        {
            // its email
            $rules['identity'] = 'required|min:4|max:32|email';
        }
        else 
        {            
            // its username. check username exist in profile table
            if(User::where('username',Input::get('identity'))->count() > 0)
            {
                // user exist so get email address
                $user = User::where('username',Input::get('identity'))->first();
                $inputs['identity'] = $user->email;
            }
            else
            {
                Session::flash('error_msg','User does not exist');
                return Redirect::route('login')->withInput(Input::except('password'));
            }
        }
        
        $v = Validator::make($inputs,$rules);
        if($v->fails())
        {
            return Redirect::route('login')->withErrors($v)->withInput(Input::except('password'));
        }
        else
        {
            try{
                $user = Sentry::getUserProvider()->findByLogin($inputs['identity']);
                $throttle = Sentry::getThrottleProvider()->findByUserId($user->id);
                $throttle->check();
                               
                Session::put('user_id',$user->id);
                // auntheticate user
                $credentials = array('email'=>$inputs['identity'],'password'=>Input::get('password'));
                
                $user = Sentry::authenticate($credentials,false);
            } catch (Cartalyst\Sentry\Users\LoginRequiredException $ex) {
                Session::flash('error_msg','Login field is required.');
                return Redirect::route('login');
            } catch (\Cartalyst\Sentry\Users\PasswordRequiredException $ex) {
                Session::flash('error_msg','Password field is required.');
                return Redirect::route('login');
            } catch (Cartalyst\Sentry\Users\WrongPasswordException $ex){
                Session::flash('error_msg','Wrong Password, Try Again.');
                return Redirect::route('login');
            } catch (Cartalyst\Sentry\Users\UserNotFoundException $ex){
                Session::flash('error_msg','User was not found');
                return Redirect::route('login');
            } catch (Cartalyst\Sentry\Users\UserNotActivatedException $ex){
                Session::flash('error_msg','User is not activated');
                return Redirect::route('login');
            } catch (Cartalyst\Sentry\Throttling\UserSuspendedException $ex){
                Session::flash('error_msg','User is suspended');
                return Redirect::route('login');
            } catch (Cartalyst\Sentry\Throttling\UserBannedException $ex){
                Session::flash('error_msg','User is Banned');
                return Redirect::route('login');
            }
            
            Session::flash('success_msg','Login Successfully');
            return Redirect::route('index');
        }
    }
    
    public function getLogout()
    {
        Sentry::logout();
        return Redirect::route('login');
    }
    
     // not use
    public function tesRegister($id=''){
//        $user = Sentry::createUser(array(
//                'first_name'        => 'Arsandi',
//                'last_name' => 'Yudiyanto',
//                'email' => 'ole16@gmail.com',
//                'password' => '23232323',
//                'activated' => true,
//            ));           
// 
//        $groupbyuser = Sentry::findGroupById(1);
//        $user->addGroup($groupbyuser);               
//        
//        $user->save();
        $menu = Menu::where('active','=','1')->get();
        
    }
    
    // not use
    public function setMenuChild($group_id,$parent='',$menu=''){
        $roles = Roles::where('group_id', '=', $group_id)->leftJoin('menus','menu_id','=','id')->where('menus.active','=','1')->where('menus.parent','=',$parent)->orderBy('menus.order','asc')->get();
        //dd($roles);exit;
        $menu = json_encode($roles);     
        $main_menu = json_decode($menu);
        $menu_parent = '';
        foreach($main_menu as $mn){
            $menunya = Menu::where('parent','=',$mn->menu_id)->count();
            if ($mn->menu_id == 1) {
                $active = 'active';
            } else {
                $active = '';
            }
                $menu_parent .= '<li class="'.$active.' treeview">';
            if($menunya == 0){
                $menu_parent .= '<a href="'.URL::to($mn->url).'"><i class="fa '.$mn->image.'"></i> <span>'.$mn->menu.'</span> <i class="fa fa-angle-left pull-right"></i></a>';
            } else {
                $menu_parent .= '<a href="'.URL::to($mn->url).'"><i class="fa '.$mn->image.'"></i> <span>'.$mn->menu.'</span> <i class="fa fa-angle-left pull-right"></i></a>';
                $menu_parent .= '<ul class="treeview-menu">';
                $menu_parent .= '<li>';
                $menu_parent .= $this->setMenuChild($group_id,$mn->menu_id,$mn->menu);
                $menu_parent .= '</li>';
                $menu_parent .= '</ul>';
            }
                $menu_parent .= '</li>';
                
        } 
        return $menu_parent;
    }
    
     // not use
    public function setMenuParent($group_id,$parent){
        $menu_parent = '';
        $menu_parent .= $this->setMenuChild($group_id,$parent);
        return $menu_parent;
    }

}
