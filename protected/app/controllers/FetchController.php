<?php

class FetchController extends Controller {
    public function fetchData(){
        $time_start1 = microtime(true);
        $data_pengajuan = Pengajuan::leftJoin('tahun_pengajuan', 'tahun_pengajuan.Kode_Tahun_Pengajuan', '=', 'pengajuan.Kode_Tahun_Pengajuan')
                    ->leftJoin('detail_pengajuan', 'detail_pengajuan.No_Pengajuan', '=', 'pengajuan.No_Pengajuan')
                    ->leftJoin('m_dosen', 'm_dosen.nidn', '=', 'pengajuan.nidn')
                    ->leftJoin('fakultas', 'fakultas.Kode_Fakultas', '=', 'm_dosen.Kode_Fakultas')
                    ->leftJoin('jurusan', 'jurusan.Kode_Jurusan', '=', 'm_dosen.Kode_Jurusan')
                    ->leftJoin('jabatan_akademik', 'jabatan_akademik.id_jabatan', '=', 'm_dosen.Id_Jabatan')                    
                    ->where('m_dosen.Kode_Jurusan', '=', 'S1SI')
//                    ->where('pengajuan.Kode_Tahun_Pengajuan',Input::get('tahun'))                    
                    ->selectRaw('pengajuan.*
                        ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)="A",detail_pengajuan.Nilai_asli,0)) as A
                        ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)="B",detail_pengajuan.Nilai_asli,0)) as B
                        ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)="C",detail_pengajuan.Nilai_asli,0)) as C
                        ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)="D",detail_pengajuan.Nilai_asli,0)) as D
                        ,nama_dosen,m_dosen.Kode_Jurusan,m_dosen.Kode_Fakultas,Nama_Fakultas,Nama_Jurusan,Tahun,Semester,Nilai_Kum_Akhir,jabatan_akademik,m_dosen.Id_Jabatan,jabatan_akademik.nilai as nilai_jabatan_akademik_awal')
                    ->groupBy('pengajuan.No_Pengajuan')
                    ->get();
        $time_exec1 = (microtime(true) - $time_start1);

        $time_exec_sum_2 = 0;
        $time_exec_sum_3 = 0;
        foreach ($data_pengajuan as $dp) {
            $time_start2 = microtime(true);
            $fact1 = Fact1::where("NIDN", $dp->NIDN)->first();
            
            $jabatan = json_decode(TDHelper::getJabatanUsulan($dp->Id_Jabatan));
            $nilai_jabatan_usulan = $jabatan->nilai_jabatan;
            $id_jabatan_usulan = $jabatan->id_jabatan;
            $nilai_jabatan_awal = $dp->nilai_jabatan_akademik_awal;
            $selisih_nilai_jabatan = $nilai_jabatan_usulan - $nilai_jabatan_awal;
            $nilai_ijazah = TDHelper::getNilaiIjazah($dp->NIDN);
            $pendidikan_terakhir = TDHelper::getPendidikanTerakhir($dp->NIDN);
            $angka_kredit_min = TDHelper::getAngkaKreditMin($id_jabatan_usulan, $selisih_nilai_jabatan);
            $data_nilai_min = json_decode($angka_kredit_min, true);
               $persen_awal = PersentaseJabatan::where('Kode_Angka_Kredit','=',$id_jabatan_usulan)->first();
            $persen_a = TDHelper::getPersentaseAkhir($dp->A, $data_nilai_min[0], $persen_awal->Persentase_Pendidikan);
            $persen_b = TDHelper::getPersentaseAkhir($dp->B, $data_nilai_min[1], $persen_awal->Persentase_Penelitian);
            $persen_c = TDHelper::getPersentaseAkhir($dp->C, $data_nilai_min[2], $persen_awal->Persentase_Pengmasy);
            $persen_d = TDHelper::getPersentaseAkhir($dp->D, $data_nilai_min[3], $persen_awal->Persentase_unsurPenunjang);

            if (count($fact1) == 0) {
                $fact1 = new Fact1;
                $fact1->NIDN = $dp->NIDN;
            }            
            $fact1->Kode_JurusanFakultas = $dp->Kode_Jurusan.$dp->Kode_Fakultas;
            $fact1->Nama_Dosen = $dp->nama_dosen;
            $fact1->Jabatan_Akademik = $dp->jabatan_akademik;
            $fact1->Jabatan_Usulan = $jabatan->nama_jabatan;
            $fact1->Nilai_Kum_Akhir = $dp->Nilai_Kum_Akhir;
            $fact1->Periode_Pengajuan = $dp->Semester.' '.$dp->Tahun;
            $fact1->Presentase_Kum_Pengajaran = $persen_a;
            $fact1->Persentase_Kum_Penelitian = $persen_b;
            $fact1->Persentase_Kum_PengMasy = $persen_c;
            $fact1->Persentase_Kum_UnsurPenunjang = $persen_d;           
            $fact1->save();
            $time_exec2 = (microtime(true) - $time_start2);

            $time_start3 = microtime(true);
            $fact2 = Fact2::where("NIDN", $dp->NIDN)->first();
            if (count($fact2) == 0) {
                $fact2 = new Fact2;
                $fact2->NIDN = $dp->NIDN;
            }
            $fact2->Kode_JurusanFakultas = $dp->Kode_Jurusan.$dp->Kode_Fakultas;
            $fact2->Nama_Dosen = $dp->nama_dosen;
            $fact2->Jabatan_Akademik = $dp->jabatan_akademik;
            $fact2->Pendidikan = $pendidikan_terakhir;
            $fact2->Kum_Pengajaran = $dp->A;
            $fact2->Kum_Penelitian = $dp->B;
            $fact2->Kum_PengMasy = $dp->C;
            $fact2->Kum_UnsurPenunjang = $dp->D;
            $fact2->save();
            $time_exec3 = (microtime(true) - $time_start3);

        }
        $time_exec_sum_2 += $time_exec2;
        $time_exec_sum_3 += $time_exec3;
        $view = View::make('laporan.oltp_warehouse')->with('time_exec1',$time_exec1)->with('time_exec2',$time_exec2)->with('time_exec3',$time_exec3);
        $this->layout = $view;
    }
}
