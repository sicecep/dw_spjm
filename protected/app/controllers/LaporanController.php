<?php

class LaporanController extends Controller {
    public function LapTridharmaOLTP()
    {

        $user = Sentry::findUserById(Session::get('user_id'));
        $nidn = Session::get('nidn');
        $tahun = Tahun::all();
        if ($user->inGroup(Sentry::findGroupById('3'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%D3SIFIK%')->get();
            $where_jurusan = " AND m_dosen.Kode_Jurusan = 'D3SI'";
        } elseif ($user->inGroup(Sentry::findGroupById('4'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1SIFIK%')->get();
            $where_jurusan = " AND m_dosen.Kode_Jurusan = 'S1SI'";
        } elseif ($user->inGroup(Sentry::findGroupById('5'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1TIFIK%')->get();
            $where_jurusan = " AND m_dosen.Kode_Jurusan = 'S1TI'";
        } else {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%FIK%')->get();
            $where_jurusan = "";
        }
        if (Input::get('submit') == 'submit') {
            $nama_jurusan = "ALL";
            $jurusan_exist = JurusanFakultas::where('Kode_JurusanFakultas', 'like', Input::get('jurusan'))->first();
            if (count($jurusan_exist) > 0) {
                $nama_jurusan = $jurusan_exist->Nama_Jurusan;
            }
            $time_start1 = microtime(true);
            $query = DB::connection('spjm')->select(DB::raw("
                  SELECT pengajuan.*
                        ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='A',detail_pengajuan.Nilai_asli,0)) as A
                        ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='B',detail_pengajuan.Nilai_asli,0)) as B
                        ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='C',detail_pengajuan.Nilai_asli,0)) as C
                        ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='D',detail_pengajuan.Nilai_asli,0)) as D
                        ,nama_dosen,m_dosen.Kode_Jurusan,m_dosen.Kode_Fakultas,Nama_Fakultas,Nama_Jurusan,Tahun,Semester,Nilai_Kum_Akhir,jabatan_akademik,m_dosen.Id_Jabatan,jabatan_akademik.nilai as nilai_jabatan_akademik_awal
                  FROM pengajuan
                  LEFT JOIN detail_pengajuan  ON detail_pengajuan.No_Pengajuan = pengajuan.No_Pengajuan
                  LEFT JOIN tahun_pengajuan ON tahun_pengajuan.Kode_Tahun_Pengajuan = pengajuan.Kode_Tahun_Pengajuan
                  LEFT JOIN m_dosen ON m_dosen.nidn = pengajuan.nidn
                  LEFT JOIN fakultas ON fakultas.Kode_Fakultas = m_dosen.Kode_Fakultas
                  LEFT JOIN jurusan ON jurusan.Kode_Jurusan = m_dosen.Kode_Jurusan
                  LEFT JOIN jabatan_akademik ON jabatan_akademik.id_jabatan =  m_dosen.Id_Jabatan
                  WHERE pengajuan.Kode_Tahun_Pengajuan = " . Input::get('tahun') . "
                  $where_jurusan
                  GROUP BY pengajuan.No_Pengajuan
            "));
            $time_exec1 = (microtime(true) - $time_start1);
            $view = View::make('laporan.tridharma')->with('jurusan', $jurusan)->with('input', Input::all())->with('data', $query)->with('tahun', $tahun)->with('time_exec', $time_exec1)->with('nama_jurusan', $nama_jurusan);
        } else {
            $view = View::make('laporan.tridharma')->with('jurusan', $jurusan)->with('tahun', $tahun);
        }
        $this->layout = $view;
    }

    public function LapTridharmaOLAP()
    {

        $user = Sentry::findUserById(Session::get('user_id'));
        $nidn = Session::get('nidn');
        $tahun = Tahun::all();
        if ($user->inGroup(Sentry::findGroupById('3'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%D3SIFIK%')->get();
            $where_jurusan = " AND fact_laporan2_3.Kode_JurusanFakultas LIKE 'D3SI%'";
        } elseif ($user->inGroup(Sentry::findGroupById('4'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1SIFIK%')->get();
            $where_jurusan = " AND fact_laporan2_3.Kode_JurusanFakultas LIKE 'S1SI%'";
        } elseif ($user->inGroup(Sentry::findGroupById('5'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1TIFIK%')->get();
            $where_jurusan = " AND fact_laporan2_3.Kode_JurusanFakultas LIKE 'S1TI%'";
        } else {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%FIK%')->get();
            $where_jurusan = "";
            if(Input::has('jurusan') && Input::get('jurusan')!=''){
                $where_jurusan = " AND fact_laporan2_3.Kode_JurusanFakultas LIKE '".Input::get('jurusan')."%'";
            }
        }
        if (Input::get('submit') == 'submit') {
            $nama_jurusan = "ALL";
            $jurusan_exist = JurusanFakultas::where('Kode_JurusanFakultas', 'like', Input::get('jurusan'))->first();
            if (count($jurusan_exist) > 0) {
                $nama_jurusan = $jurusan_exist->Nama_Jurusan;
            }
            $time_start1 = microtime(true);
            $query = DB::select(DB::raw("
                  SELECT fact_laporan2_3.*
                        ,fact_laporan2_3.Kum_Pengajaran as A
                        ,fact_laporan2_3.Kum_Penelitian as B
                        ,fact_laporan2_3.Kum_PengMasy as C
                        ,fact_laporan2_3.Kum_UnsurPenunjang as D
                        ,fact_laporan2_3.Nama_Dosen as nama_dosen
                        ,dim_jurusanfakultas.Nama_Jurusan
                        ,fact_laporan2_3.Jabatan_Akademik as jabatan_akademik
                  FROM fact_laporan2_3
                  LEFT JOIN dim_jurusanfakultas ON fact_laporan2_3.Kode_JurusanFakultas=dim_jurusanfakultas.Kode_JurusanFakultas
                  WHERE fact_laporan2_3.Kode_Tahun_Pengajuan = " . Input::get('tahun') . "
                  $where_jurusan
            "));
            $time_exec1 = (microtime(true) - $time_start1);
            $view = View::make('laporan.tridharma')->with('jurusan', $jurusan)->with('input', Input::all())->with('data', $query)->with('tahun', $tahun)->with('time_exec', $time_exec1)->with('nama_jurusan', $nama_jurusan);
        } else {
            $view = View::make('laporan.tridharma')->with('jurusan', $jurusan)->with('tahun', $tahun);
        }
        $this->layout = $view;
    }
    
    public function LapJafungOLTP(){
        $user = Sentry::findUserById(Session::get('user_id'));
        $nidn = Session::get('nidn');
        $tahun = Tahun::all();
        if ($user->inGroup(Sentry::findGroupById('3'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%D3SIFIK%')->get();
            $where_jurusan = " AND m_dosen.Kode_Jurusan = 'D3SI'";
        } elseif ($user->inGroup(Sentry::findGroupById('4'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1SIFIK%')->get();
            $where_jurusan = " AND m_dosen.Kode_Jurusan = 'S1SI'";
        } elseif ($user->inGroup(Sentry::findGroupById('5'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1TIFIK%')->get();
            $where_jurusan = " AND m_dosen.Kode_Jurusan = 'S1TI'";
        } else {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%FIK%')->get();
            $where_jurusan = "";
            if(Input::has('jurusan') && Input::get('jurusan')!=''){
                $jurex = substr(Input::get('jurusan'), 0, 4);
                $where_jurusan = " AND m_dosen.Kode_Jurusan LIKE '".$jurex."%'";
            }
        }
        if (Input::get('submit') == 'submit') {
            $nama_jurusan = "ALL";
            $jurusan_exist = JurusanFakultas::where('Kode_JurusanFakultas', 'like', Input::get('jurusan'))->first();
            if (count($jurusan_exist) > 0) {
                $nama_jurusan = $jurusan_exist->Nama_Jurusan;
            }
            $time_start1 = microtime(true);
             $query = DB::connection('spjm')->select(DB::raw("
                  SELECT pengajuan.*
                        ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='A',detail_pengajuan.Nilai_asli,0)) as A
                        ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='B',detail_pengajuan.Nilai_asli,0)) as B
                        ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='C',detail_pengajuan.Nilai_asli,0)) as C
                        ,SUM(IF(SUBSTR(detail_pengajuan.Kode_Penilaian,1,1)='D',detail_pengajuan.Nilai_asli,0)) as D
                        ,nama_dosen,m_dosen.Kode_Jurusan,m_dosen.Kode_Fakultas,Nama_Fakultas,Nama_Jurusan,Tahun,Semester,Nilai_Kum_Akhir,jabatan_akademik,m_dosen.Id_Jabatan,jabatan_akademik.nilai as nilai_jabatan_akademik_awal
                  FROM pengajuan
                  LEFT JOIN detail_pengajuan  ON detail_pengajuan.No_Pengajuan = pengajuan.No_Pengajuan
                  LEFT JOIN tahun_pengajuan ON tahun_pengajuan.Kode_Tahun_Pengajuan = pengajuan.Kode_Tahun_Pengajuan
                  LEFT JOIN m_dosen ON m_dosen.nidn = pengajuan.nidn
                  LEFT JOIN fakultas ON fakultas.Kode_Fakultas = m_dosen.Kode_Fakultas
                  LEFT JOIN jurusan ON jurusan.Kode_Jurusan = m_dosen.Kode_Jurusan
                  LEFT JOIN jabatan_akademik ON jabatan_akademik.id_jabatan =  m_dosen.Id_Jabatan
                  WHERE pengajuan.Kode_Tahun_Pengajuan = " . Input::get('tahun') . "
                  $where_jurusan
                  GROUP BY pengajuan.No_Pengajuan
            "));
          
             $data = [];
             $i = 0;
             foreach ($query as $q) {
                $jabatan = json_decode(TDHelper::getJabatanUsulan($q->Id_Jabatan));
                $nilai_jabatan_usulan = $jabatan->nilai_jabatan;
                $id_jabatan_usulan = $jabatan->id_jabatan;
                $nilai_jabatan_awal = $q->nilai_jabatan_akademik_awal;
                $selisih_nilai_jabatan = $nilai_jabatan_usulan - $nilai_jabatan_awal;
                $nilai_ijazah = TDHelper::getNilaiIjazah($q->NIDN);
                $pendidikan_terakhir = TDHelper::getPendidikanTerakhir($q->NIDN);
                $angka_kredit_min = TDHelper::getAngkaKreditMin($id_jabatan_usulan, $selisih_nilai_jabatan);
                $data_nilai_min = json_decode($angka_kredit_min, true);
                $persen_awal = PersentaseJabatan::where('Kode_Angka_Kredit', '=', $id_jabatan_usulan)->first();
                $persen_a = TDHelper::getPersentaseAkhir($q->A, $data_nilai_min[0], $persen_awal->Persentase_Pendidikan);
                $persen_b = TDHelper::getPersentaseAkhir($q->B, $data_nilai_min[1], $persen_awal->Persentase_Penelitian);
                $persen_c = TDHelper::getPersentaseAkhir($q->C, $data_nilai_min[2], $persen_awal->Persentase_Pengmasy);
                $persen_d = TDHelper::getPersentaseAkhir($q->D, $data_nilai_min[3], $persen_awal->Persentase_unsurPenunjang);
                
                $data[$i]['nidn'] = $q->NIDN;                
                $data[$i]['nama_dosen'] = $q->nama_dosen;  
                $data[$i]['jabatan_akademik'] = $q->jabatan_akademik;  
                $data[$i]['nilai_kum_akhir'] = $q->Nilai_Kum_Akhir;  
                $data[$i]['nama_jabatan'] = $jabatan->nama_jabatan;  
                $data[$i]['pendidikan_terakhir'] = $pendidikan_terakhir;  
                $data[$i]['nilai_ijazah'] = $nilai_ijazah;  
                $data[$i]['persen_a'] = $persen_a;
                $data[$i]['persen_b'] = $persen_b;
                $data[$i]['persen_c'] = $persen_c;
                $data[$i]['persen_d'] = $persen_d;
                
                $i++;
            }
            $time_exec1 = (microtime(true) - $time_start1);            
            $view = View::make('laporan.jafung')->with('jurusan', $jurusan)->with('input', Input::all())->with('data', $data)->with('tahun', $tahun)->with('time_exec', $time_exec1)->with('nama_jurusan', $nama_jurusan);
        } else {
            $view = View::make('laporan.jafung')->with('jurusan', $jurusan)->with('tahun', $tahun);
        }
        $this->layout = $view;
    }
    
    public function LapJafungOLAP(){
        $user = Sentry::findUserById(Session::get('user_id'));
        $nidn = Session::get('nidn');
        $tahun = Tahun::all();
        if ($user->inGroup(Sentry::findGroupById('3'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%D3SIFIK%')->get();
            $where_jurusan = " AND fact_laporan1.Kode_JurusanFakultas LIKE 'D3SI%'";
        } elseif ($user->inGroup(Sentry::findGroupById('4'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1SIFIK%')->get();
            $where_jurusan = " AND fact_laporan1.Kode_JurusanFakultas LIKE 'S1SI%'";
        } elseif ($user->inGroup(Sentry::findGroupById('5'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1TIFIK%')->get();
            $where_jurusan = " AND fact_laporan1.Kode_JurusanFakultas LIKE 'S1TI%'";
        } else {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%FIK%')->get();
            $where_jurusan = "";
            if(Input::has('jurusan') && Input::get('jurusan')!=''){                
                $where_jurusan = " AND fact_laporan1.Kode_JurusanFakultas LIKE '".Input::get('jurusan')."%'";
            }
        }
        if (Input::get('submit') == 'submit') {
            $nama_jurusan = "ALL";
            $jurusan_exist = JurusanFakultas::where('Kode_JurusanFakultas', 'like', Input::get('jurusan'))->first();
            if (count($jurusan_exist) > 0) {
                $nama_jurusan = $jurusan_exist->Nama_Jurusan;
            }
            $time_start1 = microtime(true);
             $query = DB::select(DB::raw("
                  SELECT fact_laporan1.*
                        ,fact_laporan1.Presentase_Kum_Pengajaran as A
                        ,fact_laporan1.Persentase_Kum_Penelitian as B
                        ,fact_laporan1.Persentase_Kum_PengMasy as C
                        ,fact_laporan1.Persentase_Kum_UnsurPenunjang as D
                        ,fact_laporan1.Nama_Dosen as nama_dosen                        
                        ,fact_laporan1.Jabatan_Akademik as jabatan_akademik
                  FROM fact_laporan1
                  LEFT JOIN dim_jurusanfakultas ON fact_laporan1.Kode_JurusanFakultas=dim_jurusanfakultas.Kode_JurusanFakultas
                  WHERE fact_laporan1.Kode_Tahun_Pengajuan = " . Input::get('tahun') . "
                  $where_jurusan
            "));
          
             $data = [];
             $i = 0;
             foreach ($query as $q) {                                                
                $nilai_ijazah = TDHelper::getNilaiIjazah($q->NIDN);
                $pendidikan_terakhir = TDHelper::getPendidikanTerakhir($q->NIDN);                                                
                
                $data[$i]['nidn'] = $q->NIDN;                
                $data[$i]['nama_dosen'] = $q->nama_dosen;  
                $data[$i]['jabatan_akademik'] = $q->jabatan_akademik;  
                $data[$i]['nilai_kum_akhir'] = $q->Nilai_Kum_Akhir;  
                $data[$i]['nama_jabatan'] = $q->Jabatan_Usulan;  
                $data[$i]['pendidikan_terakhir'] = $pendidikan_terakhir;  
                $data[$i]['nilai_ijazah'] = $nilai_ijazah;  
                $data[$i]['persen_a'] = $q->A;
                $data[$i]['persen_b'] = $q->B;
                $data[$i]['persen_c'] = $q->C;
                $data[$i]['persen_d'] = $q->D;
                
                $i++;
            }
            $time_exec1 = (microtime(true) - $time_start1);            
            $view = View::make('laporan.jafung')->with('jurusan', $jurusan)->with('input', Input::all())->with('data', $data)->with('tahun', $tahun)->with('time_exec', $time_exec1)->with('nama_jurusan', $nama_jurusan);
        } else {
            $view = View::make('laporan.jafung')->with('jurusan', $jurusan)->with('tahun', $tahun);
        }
        $this->layout = $view;
    }
    
    public function LapJabatanOLTP(){
        $user = Sentry::findUserById(Session::get('user_id'));
        $nidn = Session::get('nidn');
        $tahun = Tahun::all();
        if ($user->inGroup(Sentry::findGroupById('3'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%D3SIFIK%')->get();
            $where_jurusan = " AND b.Kode_Jurusan = 'D3SI'";
        } elseif ($user->inGroup(Sentry::findGroupById('4'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1SIFIK%')->get();
            $where_jurusan = " AND b.Kode_Jurusan = 'S1SI'";
        } elseif ($user->inGroup(Sentry::findGroupById('5'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1TIFIK%')->get();
            $where_jurusan = " AND b.Kode_Jurusan = 'S1TI'";
        } else {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%FIK%')->get();
            $where_jurusan = "";
            if(Input::has('jurusan') && Input::get('jurusan')!=''){
                $jurex = substr(Input::get('jurusan'), 0, 4);
                $where_jurusan = " AND b.Kode_Jurusan LIKE '".$jurex."%'";
            }
        }
        
        $where_tahun = '';
        if (Input::has('tahun')) {            
            if(Input::has('tahun') != ""){
                $where_tahun = " AND a.Kode_Tahun_Pengajuan = '".Input::get('tahun')."'";
            }
        }
        
        if (Input::get('submit') == 'submit') {
            $nama_jurusan = "ALL";
            $jurusan_exist = JurusanFakultas::where('Kode_JurusanFakultas', 'like', Input::get('jurusan'))->first();
            if (count($jurusan_exist) > 0) {
                $nama_jurusan = $jurusan_exist->Nama_Jurusan;
            }
            $time_start1 = microtime(true);
             $query = DB::connection('spjm')->select(DB::raw("
                  SELECT a.*,b.Nama_Dosen,b.Nilai_Kum_Akhir,c.jabatan_akademik
                  FROM pengajuan a
                  LEFT JOIN m_dosen b ON b.NIDN=a.NIDN
                  LEFT JOIN jabatan_akademik c ON c.id_jabatan=b.Id_Jabatan
                  WHERE a.NIDN IS NOT NULL
                  $where_jurusan
                  $where_tahun
            "));
             
             $data = [];
             $i = 0;
             foreach($query as $q){
                 $data[$i]['nidn'] = $q->NIDN;
                 $data[$i]['nama_dosen'] = $q->Nama_Dosen;
                 $data[$i]['jabatan_terakhir'] = $q->jabatan_akademik;
                 $data[$i]['nilai_kum_akhir'] = $q->Nilai_Kum_Akhir;
                 $i++;
             }
             $time_exec1 = (microtime(true) - $time_start1);
            $view = View::make('laporan.jabatan')->with('jurusan', $jurusan)->with('input', Input::all())->with('data', $data)->with('tahun', $tahun)->with('time_exec', $time_exec1)->with('nama_jurusan', $nama_jurusan);
        } else {
            $view = View::make('laporan.jabatan')->with('jurusan', $jurusan)->with('tahun', $tahun);;
        }
        
        $this->layout = $view;
    }
    
    public function LapJabatanOLAP(){
        $user = Sentry::findUserById(Session::get('user_id'));
        $nidn = Session::get('nidn');
        $tahun = Tahun::all();
        if ($user->inGroup(Sentry::findGroupById('3'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%D3SIFIK%')->get();
            $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = 'D3SI'";
        } elseif ($user->inGroup(Sentry::findGroupById('4'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1SIFIK%')->get();
            $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = 'S1SI'";
        } elseif ($user->inGroup(Sentry::findGroupById('5'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1TIFIK%')->get();
            $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = 'S1TI'";
        } else {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%FIK%')->get();
            $where_jurusan = "";
            if(Input::has('jurusan') && Input::get('jurusan')!=''){
                $jurex = substr(Input::get('jurusan'), 0, 4);                
                $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = '".$jurex."'";
            }
        }
        
        $where_tahun = '';
        if (Input::has('tahun')) {            
            if(Input::has('tahun') != ""){
                $where_tahun = " AND a.Kode_Tahun_Pengajuan = '".Input::get('tahun')."'";
            }
        }
        
        if (Input::get('submit') == 'submit') {
            $nama_jurusan = "ALL";
            $jurusan_exist = JurusanFakultas::where('Kode_JurusanFakultas', 'like', Input::get('jurusan'))->first();
            if (count($jurusan_exist) > 0) {
                $nama_jurusan = $jurusan_exist->Nama_Jurusan;
            }
            $time_start1 = microtime(true);
             $query = DB::select(DB::raw("
                  SELECT a.*
                  FROM fact_laporan1 a                  
                  WHERE a.NIDN IS NOT NULL
                  $where_jurusan
                  $where_tahun
            "));
             
             $data = [];
             $i = 0;
             foreach($query as $q){
                 $data[$i]['nidn'] = $q->NIDN;
                 $data[$i]['nama_dosen'] = $q->Nama_Dosen;
                 $data[$i]['jabatan_terakhir'] = $q->Jabatan_Akademik;
                 $data[$i]['nilai_kum_akhir'] = $q->Nilai_Kum_Akhir;
                 $i++;
             }
             $time_exec1 = (microtime(true) - $time_start1);
            $view = View::make('laporan.jabatan')->with('jurusan', $jurusan)->with('input', Input::all())->with('data', $data)->with('tahun', $tahun)->with('time_exec', $time_exec1)->with('nama_jurusan', $nama_jurusan);
        } else {
            $view = View::make('laporan.jabatan')->with('jurusan', $jurusan)->with('tahun', $tahun);;
        }
        
        $this->layout = $view;
    }
    
    public function GrafikTridharma(){
        $user = Sentry::findUserById(Session::get('user_id'));
        $nidn = Session::get('nidn');
        $tahun = Tahun::all();
        if ($user->inGroup(Sentry::findGroupById('3'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%D3SIFIK%')->get();
            $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = 'D3SI'";
        } elseif ($user->inGroup(Sentry::findGroupById('4'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1SIFIK%')->get();
            $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = 'S1SI'";
        } elseif ($user->inGroup(Sentry::findGroupById('5'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1TIFIK%')->get();
            $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = 'S1TI'";
        } else {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%FIK%')->get();
            $where_jurusan = "";
            if(Input::has('jurusan') && Input::get('jurusan')!=''){
                $jurex = substr(Input::get('jurusan'), 0, 4);                
                $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = '".$jurex."'";
            }
        }
        
        $where_tahun = '';
        if (Input::has('tahun')) {            
            if(Input::has('tahun') != ""){
                $where_tahun = " AND a.Kode_Tahun_Pengajuan = '".Input::get('tahun')."'";
            }
        }
        
        if (Input::get('submit') == 'submit') {
            $nama_jurusan = "ALL";
            $jurusan_exist = JurusanFakultas::where('Kode_JurusanFakultas', 'like', Input::get('jurusan'))->first();
            if (count($jurusan_exist) > 0) {
                $nama_jurusan = $jurusan_exist->Nama_Jurusan;
            }
            $time_start1 = microtime(true);
             $query = DB::select(DB::raw("
                  SELECT a.*
                  FROM fact_laporan2_3 a                  
                  WHERE a.NIDN IS NOT NULL
                  $where_jurusan
                  $where_tahun
            "));
             
             $data = [];
             $i = 0;
             foreach($query as $q){
                 $data[$i] = "[".'"'.$q->Nama_Dosen.'"'.",".$q->Kum_Pengajaran.",".$q->Kum_Penelitian.",".$q->Kum_PengMasy.",".$q->Kum_UnsurPenunjang.","."''"."]".",";                 
                 $i++;
             }
//             var_dump($data[0]);exit;
             $time_exec1 = (microtime(true) - $time_start1);
            $view = View::make('laporan.tridharma_grafik')->with('jurusan', $jurusan)->with('input', Input::all())->with('data', $data)->with('tahun', $tahun)->with('time_exec', $time_exec1)->with('nama_jurusan', $nama_jurusan);
        } else {
            $view = View::make('laporan.tridharma_grafik')->with('jurusan', $jurusan)->with('tahun', $tahun);;
        }
        
        $this->layout = $view;
    }
    
    public function GrafikJafung(){
        $user = Sentry::findUserById(Session::get('user_id'));
        $nidn = Session::get('nidn');
        $tahun = Tahun::all();
        if ($user->inGroup(Sentry::findGroupById('3'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%D3SIFIK%')->get();
            $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = 'D3SI'";
        } elseif ($user->inGroup(Sentry::findGroupById('4'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1SIFIK%')->get();
            $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = 'S1SI'";
        } elseif ($user->inGroup(Sentry::findGroupById('5'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1TIFIK%')->get();
            $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = 'S1TI'";
        } else {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%FIK%')->get();
            $where_jurusan = "";
            if(Input::has('jurusan') && Input::get('jurusan')!=''){
                $jurex = substr(Input::get('jurusan'), 0, 4);                
                $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = '".$jurex."'";
            }
        }
        
        $where_tahun = '';
        if (Input::has('tahun')) {            
            if(Input::has('tahun') != ""){
                $where_tahun = " AND a.Kode_Tahun_Pengajuan = '".Input::get('tahun')."'";
            }
        }
        
        if (Input::get('submit') == 'submit') {
            $nama_jurusan = "ALL";
            $jurusan_exist = JurusanFakultas::where('Kode_JurusanFakultas', 'like', Input::get('jurusan'))->first();
            if (count($jurusan_exist) > 0) {
                $nama_jurusan = $jurusan_exist->Nama_Jurusan;
            }
            $time_start1 = microtime(true);
             $query = DB::select(DB::raw("
                  SELECT a.*
                  FROM fact_laporan1 a                  
                  WHERE a.NIDN IS NOT NULL
                  $where_jurusan
                  $where_tahun
            "));
             
             $data = [];
             $i = 0;
             foreach($query as $q){
                 $data[$i] = "[".'"'.$q->Nama_Dosen.'"'.",".$q->Presentase_Kum_Pengajaran.",".$q->Persentase_Kum_Penelitian.",".$q->Persentase_Kum_PengMasy.",".$q->Persentase_Kum_UnsurPenunjang.","."''"."]".",";                 
                 $i++;
             }
//             var_dump($data[0]);exit;
             $time_exec1 = (microtime(true) - $time_start1);
            $view = View::make('laporan.jafung_grafik')->with('jurusan', $jurusan)->with('input', Input::all())->with('data', $data)->with('tahun', $tahun)->with('time_exec', $time_exec1)->with('nama_jurusan', $nama_jurusan);
        } else {
            $view = View::make('laporan.jafung_grafik')->with('jurusan', $jurusan)->with('tahun', $tahun);;
        }
        
        $this->layout = $view;
    }
    
    public function GrafikJabatan(){
        $user = Sentry::findUserById(Session::get('user_id'));
        $nidn = Session::get('nidn');
        $tahun = Tahun::all();
        if ($user->inGroup(Sentry::findGroupById('3'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%D3SIFIK%')->get();
            $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = 'D3SI'";
        } elseif ($user->inGroup(Sentry::findGroupById('4'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1SIFIK%')->get();
            $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = 'S1SI'";
        } elseif ($user->inGroup(Sentry::findGroupById('5'))) {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%S1TIFIK%')->get();
            $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = 'S1TI'";
        } else {
            $jurusan = JurusanFakultas::where('Kode_JurusanFakultas', 'like', '%FIK%')->get();
            $where_jurusan = "";
            if(Input::has('jurusan') && Input::get('jurusan')!=''){
                $jurex = substr(Input::get('jurusan'), 0, 4);                
                $where_jurusan = " AND SUBSTR(a.Kode_JurusanFakultas,1,4) = '".$jurex."'";
            }
        }
        
        $where_tahun = '';
        if (Input::has('tahun')) {            
            if(Input::has('tahun') != ""){
                $where_tahun = " AND a.Kode_Tahun_Pengajuan = '".Input::get('tahun')."'";
            }
        }
        
        if (Input::get('submit') == 'submit') {
            $nama_jurusan = "ALL";
            $jurusan_exist = JurusanFakultas::where('Kode_JurusanFakultas', 'like', Input::get('jurusan'))->first();
            if (count($jurusan_exist) > 0) {
                $nama_jurusan = $jurusan_exist->Nama_Jurusan;
            }
            $time_start1 = microtime(true);
             $query = DB::select(DB::raw("
                  SELECT a.*
                  FROM fact_laporan1 a                  
                  WHERE a.NIDN IS NOT NULL
                  $where_jurusan
                  $where_tahun
            "));
             
             $data = [];
             $i = 0;
             foreach($query as $q){
                 $data[$i] = "[".'"'.$q->Nama_Dosen.'"'.",".$q->Nilai_Kum_Akhir.","."''"."]".",";                 
                 $i++;
             }
//             var_dump($data[0]);exit;
             $time_exec1 = (microtime(true) - $time_start1);
            $view = View::make('laporan.jabatan_grafik')->with('jurusan', $jurusan)->with('input', Input::all())->with('data', $data)->with('tahun', $tahun)->with('time_exec', $time_exec1)->with('nama_jurusan', $nama_jurusan);
        } else {
            $view = View::make('laporan.jabatan_grafik')->with('jurusan', $jurusan)->with('tahun', $tahun);;
        }
        
        $this->layout = $view;
    }
    
    public function SQLSyntax(){
        $view = View::make('laporan.lap_sql');
        $this->layout = $view;
    }
    
    public function L4(){
        $view = View::make('laporan.jafung_olap');
        $this->layout = $view;
    }
    
    public function L5(){
        $view = View::make('laporan.jabatan_oltp');
        $this->layout = $view;
    }
    
    public function L6(){
        $view = View::make('laporan.jabatan_olap');
        $this->layout = $view;
    }
    
    public function L7(){
        $view = View::make('laporan.tridharma_grafik');
        $this->layout = $view;
    }
    
    public function L8(){
        $view = View::make('laporan.jafung_grafik');
        $this->layout = $view;
    }
    
    public function L9(){
        $view = View::make('laporan.jabatan_grafik');
        $this->layout = $view;
    }
    
    public function L10(){
        $view = View::make('laporan.lap_sql');
        $this->layout = $view;
    }
}
