@extends('index')
@section('content')
<div class="row">
        <div class="show-grid">
            <div class="col-xs-12">

                <button class="btn btn-primary">Export To PDF</button>
                <button class="btn btn-success">Export To Excel</button>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="show-grid">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading col-md-6 text-left">
                        Filter
                    </div>
                    <div class="panel-heading col-md-6 text-right">
                        &nbsp;
                    </div>
                    <div class="clear">&nbsp;</div>
                    <div class="panel-body">
                        <form role="form" action="{{ URL::current() }}">
                            <div class="form-group">
                                <label>Jurusan</label>
                                <select class="form-control" name="jurusan">
                                    <option value="">- All -</option>
                                    @if(isset($jurusan) && count($jurusan) > 0)
                                    @foreach($jurusan as $jur)
                                        <?php $selected = ''; ?>
                                        @if(isset($input) && $input['jurusan']==$jur->Kode_JurusanFakultas)
                                            <?php $selected = 'selected=selected'; ?>
                                        @endif
                                        <option value="{{ $jur->Kode_JurusanFakultas  }}" {{$selected}}>{{ $jur->Nama_Jurusan }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tahun</label>
                                <select class="form-control" name="tahun">
                                    <option value="">- All -</option>
                                    @if(isset($tahun) && count($tahun) > 0)
                                    @foreach($tahun as $thn)
                                        <?php $selected = ''; ?>
                                        @if(isset($input) && $input['tahun']==$thn->id)
                                            <?php $selected = 'selected=selected'; ?>
                                        @endif
                                        <option value="{{ $thn->id  }}" {{$selected}}>{{ $thn->deskripsi }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <button type="submit" name="submit" value="submit" class="btn btn-primary">FILTER</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>



<div class="row">
    <div class="col-lg-12">
        <h4 class="page-header">
            <p class="text-center text-bold">
                LAPORAN PER JABATAN AKADEMIK<br />                			
            </p>
        </h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">                       
        <div class="panel panel-default">            
            <div class="panel-heading col-md-6 text-left">
                    JURUSAN : {{ $nama_jurusan or '' }}
                </div>
            <div class="panel-heading col-md-6 text-right">
                    Execution Time : {{ $time_exec or '' }}
                </div>
                <div class="clear">&nbsp;</div>
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">                       
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="text-center" style="width: 60px;" rowspan="2">NO</th>
                                            <th class="text-center" style="width: 150px;" rowspan="2">NIDN</th>
                                            <th class="text-center" style="width: 150px;" rowspan="2">Nama</th>
                                            <th class="text-center" style="width: 150px;" rowspan="2">Jabatan Terakhir</th>
                                            <th class="text-center" style="width: 150px;" rowspan="2">Nilai KUM Akhir</th>                                                                               
                                        </tr>                                        
                                    </thead>
                                    <tbody>            
                                        <?php $i=1; ?>
                                        @if(isset($data) && count($data) > 0)
                                        @foreach($data as $d)
                                        <tr class="gradeA odd" role="row">
                                            <td class="text-center">{{ $i }}</td>
                                            <td class="sorting_1">{{ $d['nidn'] }}</td>
                                            <td>{{ $d['nama_dosen'] }}</td>
                                            <td>{{ $d['jabatan_terakhir'] }}</td>
                                            <td class="center">{{ $d['nilai_kum_akhir'] }}</td>                                           
                                        </tr>  
                                        <?php $i++; ?>
                                        @endforeach      
                                        @endif
                                    </tbody>

                                </table>
                            </div>
                        </div>                      
                    </div>
                </div>               
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    

</script>

@stop