@extends('index')
@section('content')
    <div class="row">
        <div class="show-grid">
            <div class="col-xs-12">
                Process OLTP To Data Warehouse
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-header">
                <button class="btn btn-primary">Process</button>
            </h4>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading col-md-6 text-left">
                    Extract Transform Load Processing
                </div>
                <div class="panel-heading col-md-6 text-right">
                    &nbsp;
                </div>
                <div class="clear">&nbsp;</div>
                <div class="panel-body">
                    <label>Batch 1 : </label>
                    Time Batch {{ $time_exec1 }} Seconds <br /><button type="button" class="btn btn-primary btn-xs">Success</button>
                    <br /> <br />
                    <label>Batch 2 : </label>
                    Time Batch {{ $time_exec2 }} Seconds <br /> <button type="button" class="btn btn-primary btn-xs">Success</button>
                    <br /> <br />
                    <label>Batch 3 : </label>
                    Time Batch {{ $time_exec3 }} Seconds <br /> <button type="button" class="btn btn-primary btn-xs">Success</button>
                    <br /> <br />
                    <b>Total Execution All Batch {{ $time_exec1 + $time_exec2 + $time_exec3 }} Seconds </b>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">


    </script>

@stop