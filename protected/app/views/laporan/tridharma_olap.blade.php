@extends('index')
@section('content')
<div class="row">    
    <div class="show-grid">
        <div class="col-xs-12">                  
            <div class="text-left">
                <button class="btn btn-primary">Export To PDF</button>
                <button class="btn btn-success">Export To Excel</button>    
            </div>
        </div>
    </div>
</div>
<div class="row">
    <div class="col-lg-12">
        <h4 class="page-header">
            <p class="text-center text-bold">
                LAPORAN KEMAJUAN TRI DHARMA  <br />
                FAKULTAS ILMU KOMPUTER	<br />							
                <b> ( OLAP ) </b>
                
            </p>
        </h4>
    </div>
</div>

<div class="row">
    <div class="col-lg-12">            
        <div class="panel panel-default">                  
            <div class="panel-heading col-md-6 text-left">    
                JURUSAN : S1 - SISTEM INFORMASI              
            </div>
            <div class="panel-heading col-md-6 text-right">    
                Execution Time : 0.023232323 seconds              
            </div>
            <div class="clear">&nbsp;</div>
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">                       
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="text-center" style="width: 60px;" rowspan="">NO</th>
                                            <th class="text-center" style="width: 80px;" rowspan="">NIDN</th>
                                            <th class="text-center" style="width: 80px;" rowspan="">JURUSAN</th>
                                            <th class="text-center" style="width: 150px;" rowspan="">NAMA DOSEN</th>
                                            <th class="text-center" style="width: 120px;" rowspan="">JABATAN AKADEMIK</th>
                                            <th class="text-center" style="width: 100px;" rowspan="">PENDIDIKAN</th>
                                            <th class="text-center" style="width: 100px;" rowspan="">KUM PENGAJARAN</th>
                                            <th class="text-center" style="width: 150px;" colspan="">KUM PENELITIAN</th>
                                            <th class="text-center" style="width: 100px;" rowspan="">KUM PENGMASY</th>  
                                            <th class="text-center" style="width: 100px;" rowspan="">KUM UNSUR PENUNJANG</th>  
                                        </tr>                                        
                                    </thead>
                                    <tbody>                                        
                                        <tr class="gradeA odd" role="row">
                                            <td class="text-center">1</td>
                                            <td class="sorting_1">0324068203</td>
                                            <td>S1SIFIK</td>
                                            <td>Dr.Dwi Nugroho, MSc</td>
                                            <td class="text-center">Lektor Kepala</td>
                                            <td class="text-center">S2</td>
                                            <td class="text-center">90</td>                                                                                      
                                            <td class="text-center">45</td>
                                            <td class="text-center">25</td>
                                            <td class="text-center">25</td>                                            
                                        </tr>  
                                        <tr class="gradeA odd" role="row">
                                            <td class="text-center">2</td>
                                            <td class="sorting_1">0326067502</td>
                                            <td>S1SIFIK</td>
                                            <td>Sayuti,M.Kom</td>
                                            <td class="text-center">Asisten Ahli</td>
                                            <td class="text-center">S2</td>
                                            <td class="text-center">11</td>                                                                                      
                                            <td class="text-center">10</td>
                                            <td class="text-center">3</td>
                                            <td class="text-center">2</td>                                            
                                        </tr> 
                                        <tr class="gradeA odd" role="row">
                                            <td class="text-center">3</td>
                                            <td class="sorting_1">0308046901</td>
                                            <td>S1SIFIK</td>
                                            <td>Titin Pramiyati,S.Kom, MSi</td>
                                            <td class="text-center">Asisten Ahli</td>
                                            <td class="text-center">S2</td>
                                            <td class="text-center">23</td>                                                                                      
                                            <td class="text-center">10</td>
                                            <td class="text-center">5</td>
                                            <td class="text-center">5</td>                                            
                                        </tr>  
                                        <tr class="gradeA odd" role="row">
                                            <td class="text-center">4</td>
                                            <td class="sorting_1">0321027401</td>
                                            <td>S1SIFIK</td>
                                            <td>Ati Zaidiah,s.Kom.,MTI</td>
                                            <td class="text-center">Asisten Ahli</td>
                                            <td class="text-center">S2</td>
                                            <td class="text-center">22</td>                                                                                      
                                            <td class="text-center">10</td>
                                            <td class="text-center">6</td>
                                            <td class="text-center">4</td>                                            
                                        </tr> 
                                        <tr class="gradeA odd" role="row">
                                            <td class="text-center">5</td>
                                            <td class="sorting_1">0321057001</td>
                                            <td>S1SIFIK</td>
                                            <td>Anita Muliawati.S.Kom.,MTI</td>
                                            <td class="text-center">Asisten Ahli</td>
                                            <td class="text-center">S2</td>
                                            <td class="text-center">16.5</td>                                                                                      
                                            <td class="text-center">10</td>
                                            <td class="text-center">3</td>
                                            <td class="text-center">3</td>                                            
                                        </tr>
                                        <tr class="gradeA odd" role="row">
                                            <td class="text-center">6</td>
                                            <td class="sorting_1">0327034701</td>
                                            <td>S1SIFIK</td>
                                            <td>Paulus Prananto.,MSc</td>
                                            <td class="text-center">Lektor Kepala 400</td>
                                            <td class="text-center">S2</td>
                                            <td class="text-center">115.5</td>                                                                                      
                                            <td class="text-center">113</td>
                                            <td class="text-center">15</td>
                                            <td class="text-center">15</td>                                            
                                        </tr>
                                        <tr class="gradeA odd" role="row">
                                            <td class="text-center">7</td>
                                            <td class="sorting_1">0420018601</td>
                                            <td>S1SIFIK</td>
                                            <td>Rio Wirawan,S.Kom.,MMSI</td>
                                            <td class="text-center">-</td>
                                            <td class="text-center">S2</td>
                                            <td class="text-center">22</td>                                                                                      
                                            <td class="text-center">10</td>
                                            <td class="text-center">2</td>
                                            <td class="text-center">2</td>                                            
                                        </tr>
                                        <tr class="gradeA odd" role="row">
                                            <td class="text-center">8</td>
                                            <td class="sorting_1">0421019501</td>
                                            <td>S1SIFIK</td>
                                            <td>Catur Nugrahaeni PD., M.Kom</td>
                                            <td class="text-center">-</td>
                                            <td class="text-center">S2</td>
                                            <td class="text-center">15</td>                                                                                      
                                            <td class="text-center">15</td>
                                            <td class="text-center">10</td>
                                            <td class="text-center">2</td>                                            
                                        </tr>
                                        <tr class="gradeA odd" role="row">
                                            <td class="text-center">9</td>
                                            <td class="sorting_1">990301537</td>
                                            <td>S1SIFIK</td>
                                            <td>Andri Sunata,S.Kom, MM, M.TI</td>
                                            <td class="text-center">Asisten Ahli </td>
                                            <td class="text-center">S2</td>
                                            <td class="text-center">16.5</td>                                                                                      
                                            <td class="text-center">10</td>
                                            <td class="text-center">6</td>
                                            <td class="text-center">3</td>                                            
                                        </tr>
                                    </tbody>

                                </table>
                            </div>
                        </div>                      
                    </div>
                </div>               
            </div>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">            
        <div class="panel panel-default">                  
            <div class="panel-heading col-md-6 text-left">    
                JURUSAN : D3 SISTEM INFORMASI              
            </div>
            <div class="panel-heading col-md-6 text-right">    
                Execution Time : 0.013232323 seconds              
            </div>
            <div class="clear">&nbsp;</div>
            <div class="panel-body">
                <div class="dataTable_wrapper">
                    <div id="dataTables-example_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">                       
                        <div class="row">
                            <div class="col-sm-12">
                                <table class="table table-striped table-bordered table-hover dataTable no-footer" id="dataTables-example" role="grid" aria-describedby="dataTables-example_info">
                                    <thead>
                                        <tr role="row">
                                            <th class="text-center" style="width: 60px;" rowspan="">NO</th>
                                            <th class="text-center" style="width: 80px;" rowspan="">NIDN</th>
                                            <th class="text-center" style="width: 80px;" rowspan="">JURUSAN</th>
                                            <th class="text-center" style="width: 150px;" rowspan="">NAMA DOSEN</th>
                                            <th class="text-center" style="width: 120px;" rowspan="">JABATAN AKADEMIK</th>
                                            <th class="text-center" style="width: 100px;" rowspan="">PENDIDIKAN</th>
                                            <th class="text-center" style="width: 100px;" rowspan="">KUM PENGAJARAN</th>
                                            <th class="text-center" style="width: 150px;" colspan="">KUM PENELITIAN</th>
                                            <th class="text-center" style="width: 100px;" rowspan="">KUM PENGMASY</th>  
                                            <th class="text-center" style="width: 100px;" rowspan="">KUM UNSUR PENUNJANG</th>  
                                        </tr>                                        
                                    </thead>
                                    <tbody>                                        
                                        <tr class="gradeA odd" role="row">
                                            <td class="text-center">1</td>
                                            <td class="sorting_1">0324068203</td>
                                            <td>D3SIFIK</td>
                                            <td>Theresiawati,S.Kom, MTI</td>
                                            <td class="text-center">Asisten Ahli</td>
                                            <td class="text-center">S2</td>
                                            <td class="text-center">22</td>                                                                                      
                                            <td class="text-center">10</td>
                                            <td class="text-center">3</td>
                                            <td class="text-center">3</td>                                            
                                        </tr>  
                                        <tr class="gradeA odd" role="row">
                                            <td class="text-center">2</td>
                                            <td class="sorting_1">0307046401</td>
                                            <td>D3SIFIK</td>
                                            <td>Erly Krisnanik,S.Kom, MM</td>
                                            <td class="text-center">Lektor 200</td>
                                            <td class="text-center">S2</td>
                                            <td class="text-center">31</td>                                                                                      
                                            <td class="text-center">20</td>
                                            <td class="text-center">5</td>
                                            <td class="text-center">5</td>                                            
                                        </tr> 
                                        <tr class="gradeA odd" role="row">
                                            <td class="text-center">3</td>
                                            <td class="sorting_1">0426619620</td>
                                            <td>D3SIFIK</td>
                                            <td>Nur Hafifah Martondong, S.Kom, MM</td>
                                            <td class="text-center">-</td>
                                            <td class="text-center">S2</td>
                                            <td class="text-center">16</td>                                                                                      
                                            <td class="text-center">0</td>
                                            <td class="text-center">2</td>
                                            <td class="text-center">3</td>                                            
                                        </tr>                                          
                                    </tbody>

                                </table>
                            </div>
                        </div>                      
                    </div>
                </div>               
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    

</script>

@stop