@extends('index')
@section('content')

    <div class="row">
        <div class="show-grid">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading col-md-6 text-left">
                        Filter
                    </div>
                    <div class="panel-heading col-md-6 text-right">
                        &nbsp;
                    </div>
                    <div class="clear">&nbsp;</div>
                    <div class="panel-body">
                        <form role="form" action="{{ URL::current() }}">
                            <div class="form-group">
                                <label>Jurusan</label>
                                <select class="form-control" name="jurusan">
                                    <option value="">- All -</option>
                                    @if(isset($jurusan))
                                    @foreach($jurusan as $jur)
                                        <?php $selected = ''; ?>
                                        @if(isset($input) && $input['jurusan']==$jur->Kode_JurusanFakultas)
                                            <?php $selected = 'selected=selected'; ?>
                                        @endif
                                        <option value="{{ $jur->Kode_JurusanFakultas  }}" {{$selected}}>{{ $jur->Nama_Jurusan }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tahun</label>
                                <select class="form-control" name="tahun">
                                    <option value="">- All -</option>
                                    @if(isset($tahun))
                                    @foreach($tahun as $thn)
                                        <?php $selected = ''; ?>
                                        @if(isset($input) && $input['tahun']==$thn->id)
                                            <?php $selected = 'selected=selected'; ?>
                                        @endif
                                        <option value="{{ $thn->id  }}" {{$selected}}>{{ $thn->deskripsi }}</option>
                                    @endforeach
                                    @endif
                                </select>
                            </div>
                            <button type="submit" name="submit" value="submit" class="btn btn-primary">FILTER</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

<div class="row">
    <div class="col-lg-12">
        <h4 class="page-header">
            <p class="text-center text-bold">
                LAPORAN KEMAJUAN TRI DHARMA  <br />                            
            </p>
        </h4>
    </div>
</div>


<div class="row">
    <div class="col-lg-12">            
        <div class="panel panel-default">                  
            <div class="panel-heading col-md-6 text-left">    
                JURUSAN : {{ $nama_jurusan or '' }}             
            </div>
            <div class="panel-heading col-md-6 text-right">    
                Execution Time : {{ $time_exec or '' }}            
            </div>
            <div class="clear">&nbsp;</div>
            <div class="clear">&nbsp;</div>
            <div class="panel-body">
               <div id="piechart" style="width: 900px; height: 500px;"></div>              
            </div>
        </div>
    </div>
</div>
<!--https://www.gstatic.com/js/loader.js-->
<script type="text/javascript" src="{{ URL::to('js/loader.js') }}"></script>
    <script type="text/javascript">
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);
      function drawChart() {

        var data = google.visualization.arrayToDataTable([
        ['Nama', 'Kum Pengajaran', 'Kum Penelitian', 'Kum Pengmasy', 'Kum Unsur Penunjang', { role: 'annotation' } ],
        @if(isset($data))
        @foreach($data as $d)
            {{ $d }}
        @endforeach
        @endif
      ]);

      var options = {
        width: 1100,
        height: 600,
        legend: { position: 'top', maxLines: 3 },
        bar: { groupWidth: '75%' },
        isStacked: true
      };

        var chart = new google.visualization.BarChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
    </script>

@stop