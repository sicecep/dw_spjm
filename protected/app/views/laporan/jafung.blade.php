@extends('index')
@section('content')
    <div class="row">
        <div class="show-grid">
            <div class="col-xs-12">

                <button class="btn btn-primary">Export To PDF</button>
                <button class="btn btn-success">Export To Excel</button>

            </div>
        </div>
    </div>

    <div class="row">
        <div class="show-grid">
            <div class="col-lg-12">
                <div class="panel panel-default">
                    <div class="panel-heading col-md-6 text-left">
                        Filter
                    </div>
                    <div class="panel-heading col-md-6 text-right">
                        &nbsp;
                    </div>
                    <div class="clear">&nbsp;</div>
                    <div class="panel-body">
                        <form role="form" action="{{ URL::current() }}">
                            <div class="form-group">
                                <label>Jurusan</label>
                                <select class="form-control" name="jurusan">
                                    <option value="">- All -</option>
                                    @foreach($jurusan as $jur)
                                        <?php $selected = ''; ?>
                                        @if(isset($input) && $input['jurusan']==$jur->Kode_JurusanFakultas)
                                            <?php $selected = 'selected=selected'; ?>
                                        @endif
                                        <option value="{{ $jur->Kode_JurusanFakultas  }}" {{$selected}}>{{ $jur->Nama_Jurusan }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Tahun</label>
                                <select class="form-control" name="tahun">
                                    @foreach($tahun as $thn)
                                        <?php $selected = ''; ?>
                                        @if(isset($input) && $input['tahun']==$thn->id)
                                            <?php $selected = 'selected=selected'; ?>
                                        @endif
                                        <option value="{{ $thn->id  }}" {{$selected}}>{{ $thn->deskripsi }}</option>
                                    @endforeach
                                </select>
                            </div>
                            <button type="submit" name="submit" value="submit" class="btn btn-primary">FILTER</button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <h4 class="page-header">
                <p class="text-center text-bold">
                    LAPORAN PENGAJUAN KENAIKAN JAFUNG <br/>                                       

                </p>
            </h4>
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading col-md-6 text-left">
                    JURUSAN : {{ $nama_jurusan or '' }}
                </div>
                <div class="panel-heading col-md-6 text-right">
                    Execution Time : {{ $time_exec or '' }}
                </div>
                <div class="clear">&nbsp;</div>
                <div class="panel-body">
                    <div class="dataTable_wrapper">
                        <div id="dataTables-example_wrapper"
                             class="dataTables_wrapper form-inline dt-bootstrap no-footer">
                            <div class="row">
                                <div class="col-sm-12">
                                    <table class="table table-striped table-bordered table-hover dataTable no-footer"
                                           id="dataTables-example" role="grid"
                                           aria-describedby="dataTables-example_info">
                                        <thead>
                                        <tr role="row">
                                            <th class="text-center" style="width: 60px;" rowspan="2">NO</th>
                                            <th class="text-center" style="width: 150px;" rowspan="2">NIDN</th>
                                            <th class="text-center" style="width: 150px;" rowspan="2">Nama</th>
                                            <th class="text-center" style="width: 150px;" rowspan="2">Jabatan Terakhir</th>
                                            <th class="text-center" style="width: 150px;" rowspan="2">Nilai KUM Akhir</th>
                                            <th class="text-center" style="width: 150px;" rowspan="2">Jabatan Usulan</th>
                                            <th class="text-center" style="width: 150px;" rowspan="2">Kualifikasi Pendidikan</th>
                                            <th class="text-center" style="width: 150px;" rowspan="2">Ijasah</th>
                                            <th class="text-center" style="width: 150px;" colspan="4">Persentase</th>                                           
                                        </tr>
                                        <tr role="row">
                                            <th style="width: 150px;">Pengajaran</th>                                            
                                            <th style="width: 150px;">Penelitian</th>                                            
                                            <th style="width: 150px;">Pengmasy</th>                                            
                                            <th style="width: 150px;">UnsPenunjang</th>                                            
                                        </tr>
                                        </thead>
                                        <tbody>
                                            
                                        @if(isset($data) && count($data) > 0)
                                        
                                            <?php $i=1; ?>
                                            @foreach($data as $d)
                                            <?php //print_r($d['nidn']);exit; ?>
                                                <tr class="gradeA odd" role="row">
                                                    <td class="text-center">{{$i}}</td>
                                                    <td class="sorting_1">{{ $d['nidn'] }}</td>
                                                    <td>{{ $d['nama_dosen'] }}</td>                                                    
                                                    <td class="text-center">{{ $d['jabatan_akademik'] }}</td>
                                                    <td class="text-center">{{ $d['nilai_kum_akhir'] }}</td>
                                                    <td class="text-center">{{ $d['nama_jabatan'] }}</td>
                                                    <td class="text-center">{{ $d['pendidikan_terakhir'] }}</td>
                                                    <td class="text-center">{{ $d['nilai_ijazah'] }}</td>
                                                    <td class="text-center">{{ $d['persen_a'].'%' }}</td>
                                                    <td class="text-center">{{ $d['persen_b'].'%' }}</td>
                                                    <td class="text-center">{{ $d['persen_c'].'%' }}</td>
                                                    <td class="text-center">{{ $d['persen_d'].'%' }}</td>
                                                </tr>
                                                <?php $i++; ?>
                                            @endforeach
                                        @endif
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>



    <script type="text/javascript">


    </script>

@stop