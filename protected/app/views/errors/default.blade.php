<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <link rel="icon" href="favicon.ico" type="image/x-icon"/>
    <title>KuDo</title>
    <style>
        @import url(//fonts.googleapis.com/css?family=Lato:700);

        body {
            width: 80%;
            margin: 0 auto 0 auto;
            font-family: "Helvetica-Light";
            background-color: #f1f1f1;
            text-align: center;
            font-size: 30px;
            color: #999;
        }

        .centered {
            position: fixed;
            top: 50%;
            left: 50%;
            margin-top: -40px;
            margin-left: -40px;
        }

        .rotate {
            -webkit-transition-duration: 0.8s;
            -moz-transition-duration: 0.8s;
            -o-transition-duration: 0.8s;
            transition-duration: 0.8s;

            -webkit-transition-property: -webkit-transform;
            -moz-transition-property: -moz-transform;
            -o-transition-property: -o-transform;
            transition-property: transform;

            overflow: hidden;

        }

        .rotate:hover {
            -webkit-transform: rotate(360deg);
            -moz-transform: rotate(360deg);
            -o-transform: rotate(360deg);
        }

    </style>
</head>
<body>
<div class="centered">
    <img class="rotate" src="{{ asset('smiley.png') }}" alt="KuDo"/> <br />{{ $error }}!
</div>
</body>
</html>