  <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                       
                        <li>
                            <a href="{{ URL::to('/') }}"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        
                        <?php 
                            $user = Sentry::findUserById(Session::get('user_id'));                            
                            $admin = Sentry::findGroupById('1');
                            $staff = Sentry::findGroupById('6');                            
                            
                        ?>
                        <?php
                        if($user->inGroup($admin)){
                        ?>
                        
                        <li>
                            <a href="{{ URL::route('fetch.data') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Fetch Data<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">
<!--                                <li>
                                    <a href="">Daftar Dosen</a>
                                </li>
                                <li>
                                    <a href="">Tambah Dosen</a>
                                </li>-->
                            </ul>
                            <!-- /.nav-second-level -->
                        </li>
                        <?php } ?>
<!--                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Reports<span class="fa arrow"></span></a>                           
                        </li>-->
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Lap. Kemajuan Tri Dharma Dosen<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">                                
                                <li>
                                    <a href="{{ URL::route('laporan.tridharma.oltp') }}">OLTP</a>
                                    <a href="{{ URL::route('laporan.tridharma.olap') }}">OLAP</a>
                                </li>
                            </ul>
                            
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Lap. Pengajuan Kenaikan Jafung<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">                                
                                <li>
                                    <a href="{{ URL::route('laporan.jafung.oltp') }}">OLTP</a>
                                    <a href="{{ URL::route('laporan.jafung.olap') }}">OLAP</a>
                                </li>
                            </ul>                            
                        </li>
                        <li>
                            <a href="#"><i class="fa fa-bar-chart-o fa-fw"></i> Lap. Per Jabatan Akademik<span class="fa arrow"></span></a>
                            <ul class="nav nav-second-level">                                
                                <li>
                                    <a href="{{ URL::route('laporan.jabatan.oltp') }}">OLTP</a>
                                    <a href="{{ URL::route('laporan.jabatan.olap') }}">OLAP</a>
                                </li>
                            </ul>                            
                        </li>
                        <li>
                            <a href="{{ URL::route('laporan.tridharma.grafik') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Lap. Kemajuan Tri Dharma<span class="fa arrow"></span></a>                                                      
                        </li>
                        <li>
                            <a href="{{ URL::route('laporan.jafung.grafik') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Lap. Kenaikan Jafung<span class="fa arrow"></span></a>                                                      
                        </li>
                        <li>
                            <a href="{{ URL::route('laporan.jabatan.grafik') }}"><i class="fa fa-bar-chart-o fa-fw"></i> Lap. Per Jabatan Akademik<span class="fa arrow"></span></a>                                                      
                        </li>
                        <li>
                            <a href="{{ URL::route('laporan.sql') }}"><i class="fa fa-bar-chart-o fa-fw"></i> SQL<span class="fa arrow"></span></a>                                                      
                        </li>
                                                         
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>