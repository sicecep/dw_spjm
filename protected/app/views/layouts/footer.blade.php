</div>
    <!-- /#wrapper -->
    
    <!-- jQuery -->
    <script src="{{ asset('bower_components/jquery/dist/jquery.min.js') }}"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('bower_components/bootstrap/dist/js/bootstrap.min.js') }}"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="{{ asset('bower_components/metisMenu/dist/metisMenu.min.js') }}"></script>

    <!-- Morris Charts JavaScript -->
    <script src="{{ asset('bower_components/raphael/raphael-min.js') }}"></script>
    <script src="{{ asset('bower_components/morrisjs/morris.min.js') }}"></script>
    <script src="{{ asset('js/morris-data.js') }}"></script>

    <!-- Jasny Bootstrap JavaScript -->
    <script src="{{ asset('js/jasny-bootstrap.min.js') }}"></script>
    
    <!-- JS Date Picker -->
    <script src="{{ asset('js/bootstrap-datepicker.min.js') }}"></script>
    
    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('dist/js/sb-admin-2.js') }}"></script>

   
    <script type="text/javascript">
$('#sandbox-container input').datepicker({
    format: 'dd-mm-yyyy',
});
</script>
</body>

</html>
